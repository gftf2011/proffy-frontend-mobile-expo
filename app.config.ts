import 'dotenv/config';

export default {
  expo: {
    name: 'proffy-frontend-mobile-expo',
    slug: 'proffy-frontend-mobile-expo',
    version: '1.0.0',
    orientation: 'portrait',
    icon: './assets/icon.png',
    splash: {
      image: './assets/splash.png',
      resizeMode: 'contain',
      backgroundColor: '#7d4de7',
    },
    updates: {
      fallbackToCacheTimeout: 0,
    },
    assetBundlePatterns: ['**/*'],
    ios: {
      supportsTablet: true,
      bundleIdentifier: 'com.gabriel.proffyExpo',
    },
    web: {
      favicon: './assets/favicon.png',
    },
    android: {
      package: 'com.gabriel.proffyExpo',
    },
    extra: {
      ip: process.env.EXPO_PACKAGER_HOSTNAME,
    },
  },
};
