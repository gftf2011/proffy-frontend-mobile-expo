import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

/**
 * Interfaces
 */
import { Teacher } from '../TeacherListContext';

export interface FavoriteTeacher {
  id: number;
  student_id: number;
  teacher_id: number;
}

export type FavoriteTeacherListContextType = {
  teachers: Teacher[];
  setTeachers: (value: Teacher[]) => void;
};

export const FavoriteTeacherListContext = createContext<FavoriteTeacherListContextType>(
  {
    teachers: [],
    setTeachers: (_value: Teacher[]) => console.warn('no teachers provided!'),
  },
);

const FavoriteTeacherListProvider: React.FC = ({ children }) => {
  const [teachers, setTeachers] = useState<Teacher[]>([]);

  return (
    <FavoriteTeacherListContext.Provider
      value={{
        teachers,
        setTeachers,
      }}
    >
      {children}
    </FavoriteTeacherListContext.Provider>
  );
};

export default FavoriteTeacherListProvider;

FavoriteTeacherListProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useFavoriteTeacherList = (): FavoriteTeacherListContextType =>
  useContext(FavoriteTeacherListContext);
