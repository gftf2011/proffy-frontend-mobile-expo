import React, { createContext, useState, useContext } from 'react';
import PropTypes from 'prop-types';

export type RemenberMeContextType = {
  remenberMe: boolean;
  setRemenberMe: (value: boolean) => void;
};

export const initialValue = {
  remenberMe: false,
  setRemenberMe: (_value: boolean): void =>
    console.warn('no remenber me provided!'),
};

export const RemenberMeContext = createContext<RemenberMeContextType>(
  initialValue,
);

const RemenberMeProvider: React.FC = ({ children }) => {
  const [remenberMe, setRemenberMe] = useState(false);
  return (
    <RemenberMeContext.Provider value={{ remenberMe, setRemenberMe }}>
      {children}
    </RemenberMeContext.Provider>
  );
};

export default RemenberMeProvider;

RemenberMeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useRemenberMe = (): RemenberMeContextType =>
  useContext(RemenberMeContext);
