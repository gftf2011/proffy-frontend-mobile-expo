import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

export interface TeacherSchedule {
  from: number;
  to: number;
  week_day: number;
}

export interface Teacher {
  id: number;
  picture?: string;
  subject: string;
  resumedName: string;
  bio?: string;
  whatsapp: string;
  schedule: TeacherSchedule[];
  cost: number;
  isFavorite: boolean;
}

export type TeacherListContextType = {
  teachers: Teacher[];
  time: string;
  subject: string;
  weekDay: number;
  setTeachers: (value: Teacher[]) => void;
  setTime: (value: string) => void;
  setSubject: (value: string) => void;
  setWeekDay: (value: number) => void;
};

export const TeacherListContext = createContext<TeacherListContextType>({
  teachers: [],
  time: '',
  subject: '',
  weekDay: 0,
  setTeachers: (_value: Teacher[]) => console.warn('no teachers provided!'),
  setTime: (_value: string) => console.warn('no time provided!'),
  setSubject: (_value: string) => console.warn('no subject provided!'),
  setWeekDay: (_value: number) => console.warn('no week day provided!'),
});

const TeacherListProvider: React.FC = ({ children }) => {
  const [teachers, setTeachers] = useState<Teacher[]>([]);
  const [subject, setSubject] = useState('');
  const [time, setTime] = useState('');
  const [weekDay, setWeekDay] = useState(0);

  return (
    <TeacherListContext.Provider
      value={{
        teachers,
        setTeachers,
        subject,
        setSubject,
        time,
        setTime,
        weekDay,
        setWeekDay,
      }}
    >
      {children}
    </TeacherListContext.Provider>
  );
};

export default TeacherListProvider;

TeacherListProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useTeacherList = (): TeacherListContextType =>
  useContext(TeacherListContext);
