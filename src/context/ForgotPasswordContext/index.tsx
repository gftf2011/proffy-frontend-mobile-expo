import React, { useState, useContext, createContext } from 'react';
import PropTypes from 'prop-types';

export type ForgotPasswordContextType = {
  token: string;
  emailTo: string;
  newPassword: string;
  forgotPasswordIndex: number;
  forgotPasswordPagesLength: number;
  setForgotPasswordPagesLength: (value: number) => void;
  setForgotPasswordIndex: (value: number) => void;
  setToken: (value: string) => void;
  setEmailTo: (value: string) => void;
  setNewPassword: (value: string) => void;
};

export const ForgotPasswordContext = createContext<ForgotPasswordContextType>({
  token: '',
  emailTo: '',
  newPassword: '',
  forgotPasswordIndex: 0,
  forgotPasswordPagesLength: 0,
  setForgotPasswordPagesLength: (_value: number) =>
    console.warn('no forgot password pages lenght provided!'),
  setForgotPasswordIndex: (_value: number) =>
    console.warn('no forgot password index provided!'),
  setToken: (_value: string): void => console.warn('no token provided!'),
  setEmailTo: (_value: string): void => console.warn('no email to provided!'),
  setNewPassword: (_value: string): void =>
    console.warn('no new password provided!'),
});

const ForgotPasswordProvider: React.FC = ({ children }) => {
  const [token, setToken] = useState('');
  const [emailTo, setEmailTo] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [forgotPasswordIndex, setForgotPasswordIndex] = useState(0);
  const [forgotPasswordPagesLength, setForgotPasswordPagesLength] = useState(0);

  return (
    <ForgotPasswordContext.Provider
      value={{
        token,
        emailTo,
        newPassword,
        forgotPasswordIndex,
        forgotPasswordPagesLength,
        setToken,
        setEmailTo,
        setNewPassword,
        setForgotPasswordIndex,
        setForgotPasswordPagesLength,
      }}
    >
      {children}
    </ForgotPasswordContext.Provider>
  );
};

export default ForgotPasswordProvider;

ForgotPasswordProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useForgotPassword = (): ForgotPasswordContextType =>
  useContext(ForgotPasswordContext);
