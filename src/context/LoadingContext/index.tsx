import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

export type LoadingContextType = {
  isLoading: boolean;
  setIsLoading: (_value: boolean) => void;
};

export const LoadingContext = createContext<LoadingContextType>({
  isLoading: false,
  setIsLoading: (_value: boolean): void => console.warn('no loading provided!'),
});

const LoadingProvider: React.FC = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <LoadingContext.Provider value={{ isLoading, setIsLoading }}>
      {children}
    </LoadingContext.Provider>
  );
};

export default LoadingProvider;

LoadingProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useLoading = (): LoadingContextType => useContext(LoadingContext);
