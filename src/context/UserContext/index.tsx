import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

export interface OfficeHour {
  id?: number;
  week_day: number | string;
  from: number | string;
  to: number | string;
  class_id?: number;
}

export type UserContextType = {
  name: string;
  lastName: string;
  resumedName: string;
  picture: string;
  isTeacher: boolean;
  officeHours: OfficeHour[];
  whatsapp: string;
  bio: string;
  setName: (value: string) => void;
  setLastName: (value: string) => void;
  setResumedName: (value: string) => void;
  setPicture: (value: string) => void;
  setIsTeacher: (value: boolean) => void;
  setOfficeHours: (value: OfficeHour[]) => void;
  setWhatsapp: (value: string) => void;
  setBio: (value: string) => void;
};

export const UserContext = createContext<UserContextType>({
  name: '',
  lastName: '',
  resumedName: '',
  picture: '',
  isTeacher: false,
  officeHours: [],
  whatsapp: '',
  bio: '',
  setName: (_value: string) => console.warn('no name provided!'),
  setLastName: (_value: string) => console.warn('no last name provided!'),
  setResumedName: (_value: string) => console.warn('no resumed name provided!'),
  setPicture: (_value: string) => console.warn('no picture provided!'),
  setIsTeacher: (_value: boolean) => console.warn('no hierarch provided!'),
  setOfficeHours: (_value: OfficeHour[]) =>
    console.warn('no office hours provided!'),
  setWhatsapp: (_value: string) => console.warn('no whatsapp provided!'),
  setBio: (_value: string) => console.warn('no bio provided!'),
});

const UserProvider: React.FC = ({ children }) => {
  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [resumedName, setResumedName] = useState('');
  const [picture, setPicture] = useState('');
  const [whatsapp, setWhatsapp] = useState('');
  const [bio, setBio] = useState('');
  const [isTeacher, setIsTeacher] = useState(false);
  const [officeHours, setOfficeHours] = useState<OfficeHour[]>([]);

  return (
    <UserContext.Provider
      value={{
        name,
        setName,
        lastName,
        setLastName,
        resumedName,
        setResumedName,
        picture,
        setPicture,
        bio,
        setBio,
        whatsapp,
        setWhatsapp,
        isTeacher,
        setIsTeacher,
        officeHours,
        setOfficeHours,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;

UserProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useUser = (): UserContextType => useContext(UserContext);
