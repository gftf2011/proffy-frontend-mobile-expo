import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

export type ClassesContextType = {
  cost: number;
  subject: string;
  setCost: (value: number) => void;
  setSubject: (value: string) => void;
};

export const ClassesContext = createContext<ClassesContextType>({
  cost: 0,
  subject: '',
  setCost: (_value: number): void => console.warn('no cost provided!'),
  setSubject: (_value: string): void => console.warn('no subject provided!'),
});

const ClassesProvider: React.FC = ({ children }) => {
  const [cost, setCost] = useState(0);
  const [subject, setSubject] = useState('');

  return (
    <ClassesContext.Provider value={{ cost, setCost, subject, setSubject }}>
      {children}
    </ClassesContext.Provider>
  );
};

export default ClassesProvider;

ClassesProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useClasses = (): ClassesContextType => useContext(ClassesContext);
