import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

export type SignInPageContextType = {
  signInIndex: number;
  signInPagesLength: number;
  setSignInPagesLength: (value: number) => void;
  setSignInIndex: (value: number) => void;
};

export const SignInPageContext = createContext<SignInPageContextType>({
  signInIndex: 0,
  signInPagesLength: 0,
  setSignInPagesLength: (_value: number): void =>
    console.warn('no sign in pages provided!'),
  setSignInIndex: (_value: number): void =>
    console.warn('no sign in pages index provided!'),
});

const SignInPageProvider: React.FC = ({ children }) => {
  const [signInIndex, setSignInIndex] = useState(0);
  const [signInPagesLength, setSignInPagesLength] = useState(3);

  return (
    <SignInPageContext.Provider
      value={{
        signInIndex,
        signInPagesLength,
        setSignInIndex,
        setSignInPagesLength,
      }}
    >
      {children}
    </SignInPageContext.Provider>
  );
};

export default SignInPageProvider;

SignInPageProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useSignInPage = (): SignInPageContextType =>
  useContext(SignInPageContext);
