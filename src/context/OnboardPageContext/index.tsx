import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

export type OnboardPageContextType = {
  onboardIndex: number;
  onboardPagesLength: number;
  setOnboardPagesLength: (value: number) => void;
  setOnboardIndex: (value: number) => void;
};

export const OnboardPageContext = createContext<OnboardPageContextType>({
  onboardIndex: 0,
  onboardPagesLength: 0,
  setOnboardPagesLength: (_value: number): void =>
    console.warn('no onboard pages provided!'),
  setOnboardIndex: (_value: number): void =>
    console.warn('no onboard pages index provided!'),
});

const OnboardPageProvider: React.FC = ({ children }) => {
  const [onboardIndex, setOnboardIndex] = useState(0);
  const [onboardPagesLength, setOnboardPagesLength] = useState(2);

  return (
    <OnboardPageContext.Provider
      value={{
        onboardIndex,
        onboardPagesLength,
        setOnboardIndex,
        setOnboardPagesLength,
      }}
    >
      {children}
    </OnboardPageContext.Provider>
  );
};

export default OnboardPageProvider;

OnboardPageProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useOnboardPage = (): OnboardPageContextType =>
  useContext(OnboardPageContext);
