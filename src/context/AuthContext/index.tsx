import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

export type AuthContextType = {
  email: string;
  emailToEdit: string;
  password: string;
  setEmail: (value: string) => void;
  setEmailToEdit: (value: string) => void;
  setPassword: (value: string) => void;
};

export const AuthContext = createContext<AuthContextType>({
  email: '',
  emailToEdit: '',
  password: '',
  setEmail: (_value: string): void => console.warn('no email provided!'),
  setEmailToEdit: (_value: string): void =>
    console.warn('no email to edit provided!'),
  setPassword: (_value: string): void => console.warn('no password provided!'),
});

const AuthProvider: React.FC = ({ children }) => {
  const [email, setEmail] = useState('');
  const [emailToEdit, setEmailToEdit] = useState('');
  const [password, setPassword] = useState('');

  return (
    <AuthContext.Provider
      value={{
        email,
        setEmail,
        emailToEdit,
        setEmailToEdit,
        password,
        setPassword,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useAuth = (): AuthContextType => useContext(AuthContext);
