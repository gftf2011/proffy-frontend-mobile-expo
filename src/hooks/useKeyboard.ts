import { useState, useEffect } from 'react';
import { KeyboardEvent, Keyboard } from 'react-native';

const useKeyboard = (): number[] => {
  /**
   * States
   */
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  /**
   * Handlers
   */
  const handleKeyboardDidShow = (event: KeyboardEvent): void => {
    setKeyboardHeight(event.endCoordinates.height);
  };

  const handleKeyboardDidHide = (): void => {
    setKeyboardHeight(0);
  };

  /**
   * Effects
   */
  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', handleKeyboardDidShow);
    Keyboard.addListener('keyboardDidHide', handleKeyboardDidHide);
    return (): void => {
      Keyboard.removeListener('keyboardDidShow', handleKeyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', handleKeyboardDidHide);
    };
  }, []);

  return [keyboardHeight];
};

export default useKeyboard;
