import useSWR from 'swr';

/**
 * Services
 */
import { ibgeApi } from '../services';

/**
 * Contants
 */
import { UF_LIST_BY_ID } from '../constants/data';

/**
 * Interfaces
 */
interface IbgeCities {
  nome: string;
}

const useFetchCitiesByUfs = (uf: string) => {
  const ufSelected = UF_LIST_BY_ID.find(item => item.value === uf);
  const ufId = ufSelected?.label;
  const { data, error, isValidating } = useSWR(
    `/localidades/estados/${ufId}/municipios`,
    async (url: string) => {
      const response = await ibgeApi().get(url);

      const cities = response.data as IbgeCities[];

      const mappedCities = cities.map(city => {
        return { label: city.nome, value: city.nome };
      });

      return mappedCities;
    },
  );

  return { data, error, isValidating };
};

export default useFetchCitiesByUfs;
