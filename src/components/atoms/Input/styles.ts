import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  input: {
    width: '100%',
    borderWidth: 2,
    backgroundColor: COLORS.CULTURED,
    fontFamily: 'Poppins_400Regular',
    color: COLORS.DARK_BLUE_GRAY,
    fontSize: 14,
    lineHeight: 24,
    paddingHorizontal: 24,
  },
});

export const inputSizeStyle = StyleSheet.create({
  small: {
    height: 24,
  },
  medium: {
    height: 54,
  },
  high: {
    height: 360,
    textAlignVertical: 'top',
    paddingVertical: 24,
  },
});

export const inputCornerStyle = StyleSheet.create({
  noCorner: {
    borderColor: COLORS.CULTURED,
  },
  withCorner: {
    borderColor: COLORS.MAGNOLIA,
  },
});

export const inputBorderStyle = StyleSheet.create({
  noBorder: {
    borderRadius: 8,
  },
  withBorder: {
    borderRadius: 0,
  },
});

export default styles;
