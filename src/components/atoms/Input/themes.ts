import { TextStyle } from 'react-native';

import {
  INPUT_SIZE_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_BORDER_TYPE,
} from './interfaces';

import { inputBorderStyle, inputCornerStyle, inputSizeStyle } from './styles';

export const handleInputSizeType = (
  inputSizeType?: INPUT_SIZE_TYPE,
): TextStyle => {
  const { SMALL, MEDIUM, HIGH } = INPUT_SIZE_TYPE;
  switch (inputSizeType) {
    case SMALL:
      return inputSizeStyle.small;
    case MEDIUM:
      return inputSizeStyle.medium;
    case HIGH:
      return inputSizeStyle.high;
    default:
      return inputSizeStyle.medium;
  }
};

export const handleInputCornerType = (
  inputCornerType?: INPUT_CORNER_TYPE,
): TextStyle => {
  const { NO_CORNER, WITH_CORNER } = INPUT_CORNER_TYPE;
  switch (inputCornerType) {
    case NO_CORNER:
      return inputCornerStyle.noCorner;
    case WITH_CORNER:
      return inputCornerStyle.withCorner;
    default:
      return inputCornerStyle.noCorner;
  }
};

export const handleInputBorderType = (
  inputBorderType?: INPUT_BORDER_TYPE,
): TextStyle => {
  const { NO_BORDER, WITH_BORDER } = INPUT_BORDER_TYPE;
  switch (inputBorderType) {
    case NO_BORDER:
      return inputBorderStyle.noBorder;
    case WITH_BORDER:
      return inputBorderStyle.withBorder;
    default:
      return inputBorderStyle.withBorder;
  }
};
