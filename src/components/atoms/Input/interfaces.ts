import { TextInputProps } from 'react-native';

/**
 * Enums
 */
export enum INPUT_SIZE_TYPE {
  SMALL = 0,
  MEDIUM = 1,
  HIGH = 2,
}

export enum INPUT_CORNER_TYPE {
  NO_CORNER = 0,
  WITH_CORNER = 1,
}

export enum INPUT_BORDER_TYPE {
  NO_BORDER = 0,
  WITH_BORDER = 1,
}

export interface InputProps extends TextInputProps {
  inputSize?: INPUT_SIZE_TYPE;
  inputCorner?: INPUT_CORNER_TYPE;
  inputBorder?: INPUT_BORDER_TYPE;
}
