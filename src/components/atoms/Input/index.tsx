import React from 'react';
import { TextInput } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interfaces
 */
import { InputProps } from './interfaces';

/**
 * Themes
 */
import {
  handleInputBorderType,
  handleInputCornerType,
  handleInputSizeType,
} from './themes';

/**
 * Props
 */
import PropTypes from './propTypes';

const Input: React.FC<InputProps> = ({
  inputSize,
  inputCorner,
  inputBorder,
  ...rest
}) => {
  return (
    <TextInput
      {...rest}
      style={[
        styles.input,
        handleInputSizeType(inputSize),
        handleInputCornerType(inputCorner),
        handleInputBorderType(inputBorder),
      ]}
    />
  );
};

export default Input;

Input.propTypes = PropTypes;
