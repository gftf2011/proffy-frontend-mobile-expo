import React from 'react';
import { Picker } from '@react-native-community/picker';
import { View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interfaces
 */
import { SelectProps, PickerItem } from './interfaces';

/**
 * Themes
 */
import {
  handleSelectBorderType,
  handleSelectCornerType,
  handleSelectSizeType,
} from './themes';

/**
 * Props
 */
import PropTypes from './propTypes';

const Select: React.FC<SelectProps> = ({
  options,
  selectSize,
  selectCorner,
  selectBorder,
  ...rest
}) => {
  return (
    <View
      style={[
        styles.selectWrapper,
        handleSelectBorderType(selectBorder),
        handleSelectCornerType(selectCorner),
        handleSelectSizeType(selectSize),
      ]}
    >
      <Picker
        {...rest}
        mode="dialog"
        style={styles.select}
        itemStyle={styles.selectItem}
      >
        {options.map((item: PickerItem) => (
          <Picker.Item key={item.value} label={item.label} value={item.value} />
        ))}
      </Picker>
    </View>
  );
};

export default Select;

Select.propTypes = PropTypes;
