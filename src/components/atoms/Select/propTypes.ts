import PropTypes from 'prop-types';
import { ValidationMap } from 'react';

import {
  PickerItem,
  SELECT_BORDER_TYPE,
  SELECT_CORNER_TYPE,
  SELECT_SIZE_TYPE,
} from './interfaces';

export default {
  options: PropTypes.arrayOf(
    PropTypes.shape<ValidationMap<PickerItem>>({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired,
    }).isRequired,
  ).isRequired,
  selectSize: PropTypes.oneOf([
    SELECT_SIZE_TYPE.SMALL,
    SELECT_SIZE_TYPE.MEDIUM,
  ]),
  selectCorner: PropTypes.oneOf([
    SELECT_CORNER_TYPE.NO_CORNER,
    SELECT_CORNER_TYPE.WITH_CORNER,
  ]),
  selectBorder: PropTypes.oneOf([
    SELECT_BORDER_TYPE.NO_BORDER,
    SELECT_BORDER_TYPE.WITH_BORDER,
  ]),
};
