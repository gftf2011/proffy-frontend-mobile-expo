import { ViewStyle } from 'react-native';

import {
  SELECT_SIZE_TYPE,
  SELECT_CORNER_TYPE,
  SELECT_BORDER_TYPE,
} from './interfaces';

import {
  selectBorderStyle,
  selectCornerStyle,
  selectSizeStyle,
} from './styles';

export const handleSelectSizeType = (
  selectSizeType?: SELECT_SIZE_TYPE,
): ViewStyle => {
  const { SMALL, MEDIUM } = SELECT_SIZE_TYPE;
  switch (selectSizeType) {
    case SMALL:
      return selectSizeStyle.small;
    case MEDIUM:
      return selectSizeStyle.medium;
    default:
      return selectSizeStyle.medium;
  }
};

export const handleSelectCornerType = (
  selectCornerType?: SELECT_CORNER_TYPE,
): ViewStyle => {
  const { NO_CORNER, WITH_CORNER } = SELECT_CORNER_TYPE;
  switch (selectCornerType) {
    case NO_CORNER:
      return selectCornerStyle.noCorner;
    case WITH_CORNER:
      return selectCornerStyle.withCorner;
    default:
      return selectCornerStyle.noCorner;
  }
};

export const handleSelectBorderType = (
  selectBorderType?: SELECT_BORDER_TYPE,
): ViewStyle => {
  const { NO_BORDER, WITH_BORDER } = SELECT_BORDER_TYPE;
  switch (selectBorderType) {
    case NO_BORDER:
      return selectBorderStyle.noBorder;
    case WITH_BORDER:
      return selectBorderStyle.withBorder;
    default:
      return selectBorderStyle.withBorder;
  }
};
