import { PickerProps } from '@react-native-community/picker/typings/Picker';

/**
 * Enums
 */
export enum SELECT_SIZE_TYPE {
  SMALL = 0,
  MEDIUM = 1,
}

export enum SELECT_CORNER_TYPE {
  NO_CORNER = 0,
  WITH_CORNER = 1,
}

export enum SELECT_BORDER_TYPE {
  NO_BORDER = 0,
  WITH_BORDER = 1,
}

/**
 * Interfaces
 */
export interface PickerItem {
  label: string;
  value: string | number;
}

export interface SelectProps extends PickerProps {
  options: PickerItem[];
  selectSize?: SELECT_SIZE_TYPE;
  selectCorner?: SELECT_CORNER_TYPE;
  selectBorder?: SELECT_BORDER_TYPE;
}
