import PropTypes from 'prop-types';

import {
  TIMEPICKER_BORDER_TYPE,
  TIMEPICKER_CORNER_TYPE,
  TIMEPICKER_SIZE_TYPE,
} from './interfaces';

export default {
  timePickerSize: PropTypes.oneOf([
    TIMEPICKER_SIZE_TYPE.SMALL,
    TIMEPICKER_SIZE_TYPE.MEDIUM,
  ]),
  timePickerCorner: PropTypes.oneOf([
    TIMEPICKER_CORNER_TYPE.NO_CORNER,
    TIMEPICKER_CORNER_TYPE.WITH_CORNER,
  ]),
  timePickerBorder: PropTypes.oneOf([
    TIMEPICKER_BORDER_TYPE.NO_BORDER,
    TIMEPICKER_BORDER_TYPE.WITH_BORDER,
  ]),
  show: PropTypes.bool,
  text: PropTypes.string,
  onPress: PropTypes.func.isRequired,
};
