import { ViewStyle } from 'react-native';

import {
  TIMEPICKER_SIZE_TYPE,
  TIMEPICKER_CORNER_TYPE,
  TIMEPICKER_BORDER_TYPE,
} from './interfaces';

import {
  timePickerBorderStyle,
  timePickerCornerStyle,
  timePickerSizeStyle,
} from './styles';

export const handleTimePickerSizeType = (
  timePickerSizeType?: TIMEPICKER_SIZE_TYPE,
): ViewStyle => {
  const { SMALL, MEDIUM } = TIMEPICKER_SIZE_TYPE;
  switch (timePickerSizeType) {
    case SMALL:
      return timePickerSizeStyle.small;
    case MEDIUM:
      return timePickerSizeStyle.medium;
    default:
      return timePickerSizeStyle.medium;
  }
};

export const handleTimePickerCornerType = (
  timePickerCornerType?: TIMEPICKER_CORNER_TYPE,
): ViewStyle => {
  const { NO_CORNER, WITH_CORNER } = TIMEPICKER_CORNER_TYPE;
  switch (timePickerCornerType) {
    case NO_CORNER:
      return timePickerCornerStyle.noCorner;
    case WITH_CORNER:
      return timePickerCornerStyle.withCorner;
    default:
      return timePickerCornerStyle.noCorner;
  }
};

export const handleTimePickerBorderType = (
  timePickerBorderType?: TIMEPICKER_BORDER_TYPE,
): ViewStyle => {
  const { NO_BORDER, WITH_BORDER } = TIMEPICKER_BORDER_TYPE;
  switch (timePickerBorderType) {
    case NO_BORDER:
      return timePickerBorderStyle.noBorder;
    case WITH_BORDER:
      return timePickerBorderStyle.withBorder;
    default:
      return timePickerBorderStyle.withBorder;
  }
};
