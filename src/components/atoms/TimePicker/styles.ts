import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  timePickerContainer: {
    width: '100%',
    borderWidth: 2,
    backgroundColor: COLORS.CULTURED,
    paddingHorizontal: 24,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  timePickerText: {
    fontFamily: 'Poppins_400Regular',
    color: COLORS.DARK_BLUE_GRAY,
    fontSize: 14,
    lineHeight: 24,
  },
  timePicker: {
    flex: 1,
  },
});

export const timePickerSizeStyle = StyleSheet.create({
  small: {
    height: 24,
  },
  medium: {
    height: 54,
  },
});

export const timePickerCornerStyle = StyleSheet.create({
  noCorner: {
    borderColor: COLORS.CULTURED,
  },
  withCorner: {
    borderColor: COLORS.MAGNOLIA,
  },
});

export const timePickerBorderStyle = StyleSheet.create({
  noBorder: {
    borderRadius: 8,
  },
  withBorder: {
    borderRadius: 0,
  },
});

export default styles;
