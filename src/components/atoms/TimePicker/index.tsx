import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

/**
 * Styles
 */
import styles from './styles';

/**
 * Themes
 */
import {
  handleTimePickerBorderType,
  handleTimePickerCornerType,
  handleTimePickerSizeType,
} from './themes';

/**
 * Interfaces
 */
import { TimePickerProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const TimePicker: React.FC<TimePickerProps> = ({
  timePickerBorder,
  timePickerCorner,
  timePickerSize,
  show = false,
  text = 'Selecione uma opção',
  onPress,
  ...rest
}) => {
  return (
    <>
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.timePickerContainer,
          handleTimePickerSizeType(timePickerSize),
          handleTimePickerCornerType(timePickerCorner),
          handleTimePickerBorderType(timePickerBorder),
        ]}
      >
        <Text numberOfLines={1} style={styles.timePickerText}>
          {text}
        </Text>
      </TouchableOpacity>
      {show === true && <DateTimePicker {...rest} style={styles.timePicker} />}
    </>
  );
};

export default TimePicker;

TimePicker.propTypes = PropTypes;
