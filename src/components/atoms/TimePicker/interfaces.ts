import { GestureResponderEvent } from 'react-native';
import {
  IOSNativeProps,
  AndroidNativeProps,
} from '@react-native-community/datetimepicker';

/**
 * Enums
 */
export enum TIMEPICKER_SIZE_TYPE {
  SMALL = 0,
  MEDIUM = 1,
}

export enum TIMEPICKER_CORNER_TYPE {
  NO_CORNER = 0,
  WITH_CORNER = 1,
}

export enum TIMEPICKER_BORDER_TYPE {
  NO_BORDER = 0,
  WITH_BORDER = 1,
}

/**
 * Interfaces
 */
export interface TimePickerProps
  extends Readonly<IOSNativeProps & AndroidNativeProps> {
  timePickerSize?: TIMEPICKER_SIZE_TYPE;
  timePickerCorner?: TIMEPICKER_CORNER_TYPE;
  timePickerBorder?: TIMEPICKER_BORDER_TYPE;
  show?: boolean;
  text?: string;
  onPress: (event?: GestureResponderEvent) => void;
}
