import { StyleProp, TextStyle } from 'react-native';

/**
 * Interfaces
 */
export enum IconType {
  IONICONS = 'ionicons',
  FEATHER = 'feather',
  FONT_AWESOME = 'fontAwesome',
}

/**
 * Enum
 */
export interface IconProps {
  type: IconType;
  color: string;
  size: number;
  name: string;
  style?: StyleProp<TextStyle>;
}
