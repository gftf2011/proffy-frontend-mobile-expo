import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import { IconType } from './interfaces';

export default {
  type: PropTypes.oneOf([
    IconType.FEATHER,
    IconType.IONICONS,
    IconType.FONT_AWESOME,
  ]).isRequired,
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  style: ViewPropTypes.style,
};
