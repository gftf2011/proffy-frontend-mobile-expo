import React, { ReactNode } from 'react';
import { Ionicons, Feather, FontAwesome } from '@expo/vector-icons';

/**
 * Interfaces
 */
import { IconProps, IconType } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const Icon: React.FC<IconProps> = ({ type, color, size, name, style }) => {
  const handleIconSet = (iconType: IconType): ReactNode => {
    switch (iconType) {
      case IconType.FEATHER:
        return <Feather name={name} color={color} size={size} style={style} />;
      case IconType.IONICONS:
        return <Ionicons name={name} color={color} size={size} style={style} />;
      case IconType.FONT_AWESOME:
        return (
          <FontAwesome name={name} color={color} size={size} style={style} />
        );
      default:
        return <></>;
    }
  };

  return <>{handleIconSet(type)}</>;
};

export default Icon;

Icon.propTypes = PropTypes;
