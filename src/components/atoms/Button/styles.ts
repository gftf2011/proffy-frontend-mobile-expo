import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  buttonDefault: {
    borderRadius: 8,
    height: 56,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  round40: {
    borderRadius: 20,
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    borderRadius: 8,
    padding: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export const colors = StyleSheet.create({
  primary: {
    backgroundColor: COLORS.PLUMP_PURPLE,
  },
  secondary: {
    backgroundColor: COLORS.MALACHITE,
  },
  error: {
    backgroundColor: COLORS.IMPERIAL_RED,
  },
  transparent: {
    backgroundColor: 'transparent',
  },
});

export const transparency = StyleSheet.create({
  clickable: {
    opacity: 1,
  },
  unclickable: {
    opacity: 0.668,
  },
});

export default styles;
