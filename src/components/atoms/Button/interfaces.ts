import { ReactNode } from 'react';
import { TouchableOpacityProps } from 'react-native';

/**
 * Enums
 */
export enum COLORS {
  PRIMARY = 0,
  SECONDARY = 1,
  ERROR = 2,
  TRANSPARENT = 3,
}

export interface ButtonProps extends TouchableOpacityProps {
  buttonColor?: COLORS;
  isDisabled?: boolean;
  children: ReactNode;
}
