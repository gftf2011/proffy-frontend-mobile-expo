import React from 'react';
import { TouchableOpacity } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Themes
 */
import { handleButtonColor, handleButtonDisabled } from './themes';

/**
 * Interfaces
 */
import { ButtonProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const Button: React.FC<ButtonProps> = ({
  children,
  isDisabled = false,
  buttonColor,
  ...rest
}) => {
  return (
    <TouchableOpacity
      {...rest}
      disabled={isDisabled}
      style={[
        styles.buttonDefault,
        handleButtonColor(buttonColor),
        handleButtonDisabled(isDisabled),
      ]}
    >
      {children}
    </TouchableOpacity>
  );
};

export const ButtonRounded: React.FC<ButtonProps> = ({
  children,
  isDisabled = false,
  buttonColor,
  ...rest
}) => {
  return (
    <TouchableOpacity
      {...rest}
      disabled={isDisabled}
      style={[
        styles.round40,
        handleButtonColor(buttonColor),
        handleButtonDisabled(isDisabled),
      ]}
    >
      {children}
    </TouchableOpacity>
  );
};

export const ButtonWrapper: React.FC<ButtonProps> = ({
  children,
  isDisabled = false,
  buttonColor,
  ...rest
}) => {
  return (
    <TouchableOpacity
      {...rest}
      disabled={isDisabled}
      style={[
        styles.wrapper,
        handleButtonColor(buttonColor),
        handleButtonDisabled(isDisabled),
      ]}
    >
      {children}
    </TouchableOpacity>
  );
};

export default Button;

Button.propTypes = PropTypes;
ButtonRounded.propTypes = PropTypes;
ButtonWrapper.propTypes = PropTypes;
