import { ViewStyle } from 'react-native';

import { COLORS } from './interfaces';

import { colors, transparency } from './styles';

export const handleButtonColor = (buttonColors?: COLORS): ViewStyle => {
  const { PRIMARY, SECONDARY, ERROR, TRANSPARENT } = COLORS;
  switch (buttonColors) {
    case PRIMARY:
      return colors.primary;
    case SECONDARY:
      return colors.secondary;
    case ERROR:
      return colors.error;
    case TRANSPARENT:
      return colors.transparent;
    default:
      return colors.primary;
  }
};

export const handleButtonDisabled = (isDisabled: boolean): ViewStyle => {
  if (isDisabled) {
    return transparency.unclickable;
  }
  return transparency.clickable;
};
