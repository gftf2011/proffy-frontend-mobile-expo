import PropTypes from 'prop-types';

import { COLORS } from './interfaces';

export default {
  buttonColor: PropTypes.oneOf([
    COLORS.PRIMARY,
    COLORS.SECONDARY,
    COLORS.ERROR,
    COLORS.TRANSPARENT,
  ]),
  children: PropTypes.node.isRequired,
  isDisabled: PropTypes.bool,
};
