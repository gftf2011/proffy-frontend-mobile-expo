import PropTypes from 'prop-types';

import {
  INPUT_BORDER_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_SIZE_TYPE,
  MASK_TYPE,
} from './interfaces';

export default {
  inputSize: PropTypes.oneOf([INPUT_SIZE_TYPE.SMALL, INPUT_SIZE_TYPE.MEDIUM]),
  inputCorner: PropTypes.oneOf([
    INPUT_CORNER_TYPE.NO_CORNER,
    INPUT_CORNER_TYPE.WITH_CORNER,
  ]),
  inputBorder: PropTypes.oneOf([
    INPUT_BORDER_TYPE.NO_BORDER,
    INPUT_BORDER_TYPE.WITH_BORDER,
  ]),
  maskType: PropTypes.oneOf([
    MASK_TYPE.BRL_CELLPHONE,
    MASK_TYPE.INTERNATIONAL_CELLPHONE,
    MASK_TYPE.CPF,
    MASK_TYPE.CURRENCY,
    MASK_TYPE.CNPJ,
  ]).isRequired,
};
