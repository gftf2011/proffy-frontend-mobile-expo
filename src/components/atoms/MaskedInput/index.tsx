import React from 'react';
import { TextInputMask } from 'react-native-masked-text';

/**
 * Styles
 */
import styles from './styles';

/**
 * Themes
 */
import {
  handleInputBorderType,
  handleInputCornerType,
  handleInputSizeType,
  handleMaskOption,
  handleMaskType,
} from './themes';

/**
 * Interfaces
 */
import { MaskedInputProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const MaskedInput: React.FC<MaskedInputProps> = ({
  inputSize,
  inputCorner,
  inputBorder,
  maskType,
  ...rest
}) => {
  return (
    <TextInputMask
      {...rest}
      style={[
        styles.input,
        handleInputSizeType(inputSize),
        handleInputCornerType(inputCorner),
        handleInputBorderType(inputBorder),
      ]}
      type={handleMaskType(maskType)}
      options={handleMaskOption(maskType)}
    />
  );
};

export default MaskedInput;

MaskedInput.propTypes = PropTypes;
