import { TextStyle } from 'react-native';
import {
  TextInputMaskOptionProp,
  TextInputMaskTypeProp,
} from 'react-native-masked-text';

import {
  INPUT_SIZE_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_BORDER_TYPE,
  MASK_TYPE,
} from './interfaces';

import { inputBorderStyle, inputCornerStyle, inputSizeStyle } from './styles';

export const handleInputSizeType = (
  inputSizeType?: INPUT_SIZE_TYPE,
): TextStyle => {
  const { SMALL, MEDIUM } = INPUT_SIZE_TYPE;
  switch (inputSizeType) {
    case SMALL:
      return inputSizeStyle.small;
    case MEDIUM:
      return inputSizeStyle.medium;
    default:
      return inputSizeStyle.medium;
  }
};

export const handleInputCornerType = (
  inputCornerType?: INPUT_CORNER_TYPE,
): TextStyle => {
  const { NO_CORNER, WITH_CORNER } = INPUT_CORNER_TYPE;
  switch (inputCornerType) {
    case NO_CORNER:
      return inputCornerStyle.noCorner;
    case WITH_CORNER:
      return inputCornerStyle.withCorner;
    default:
      return inputCornerStyle.noCorner;
  }
};

export const handleInputBorderType = (
  inputBorderType?: INPUT_BORDER_TYPE,
): TextStyle => {
  const { NO_BORDER, WITH_BORDER } = INPUT_BORDER_TYPE;
  switch (inputBorderType) {
    case NO_BORDER:
      return inputBorderStyle.noBorder;
    case WITH_BORDER:
      return inputBorderStyle.withBorder;
    default:
      return inputBorderStyle.withBorder;
  }
};

export const handleMaskType = (typeMask?: MASK_TYPE): TextInputMaskTypeProp => {
  switch (typeMask) {
    case MASK_TYPE.BRL_CELLPHONE:
      return 'cel-phone';
    case MASK_TYPE.INTERNATIONAL_CELLPHONE:
      return 'cel-phone';
    case MASK_TYPE.CPF:
      return 'cpf';
    case MASK_TYPE.CURRENCY:
      return 'money';
    case MASK_TYPE.CNPJ:
      return 'cnpj';
    default:
      return 'custom';
  }
};

export const handleMaskOption = (
  typeMask?: MASK_TYPE,
): TextInputMaskOptionProp => {
  switch (typeMask) {
    case MASK_TYPE.BRL_CELLPHONE:
      return {
        maskType: 'BRL',
        withDDD: true,
        dddMask: '(99) ',
      };
    case MASK_TYPE.INTERNATIONAL_CELLPHONE:
      return {
        maskType: 'INTERNATIONAL',
      };
    case MASK_TYPE.CPF || MASK_TYPE.CNPJ:
      return {};
    case MASK_TYPE.CURRENCY:
      return {
        precision: 2,
        separator: ',',
        delimiter: '.',
        unit: 'R$',
        suffixUnit: '',
      };
    default:
      return {};
  }
};
