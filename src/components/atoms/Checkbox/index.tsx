import React from 'react';
import CheckBox from '@react-native-community/checkbox';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Interfaces
 */
import CheckBoxProps from './interfaces';

/**
 * Styles
 */
import styles from './styles';

const Checkbox: React.FC<CheckBoxProps> = ({ ...rest }) => {
  return (
    <CheckBox
      {...rest}
      style={styles.primary}
      tintColors={{ true: COLORS.MALACHITE, false: COLORS.WHITE }}
    />
  );
};

export default Checkbox;
