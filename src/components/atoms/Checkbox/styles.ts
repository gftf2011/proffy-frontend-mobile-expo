import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  primary: {
    color: COLORS.WHITE,
  },
});

export default styles;
