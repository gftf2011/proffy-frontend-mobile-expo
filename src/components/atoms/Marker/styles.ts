import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  marker: {
    height: 6,
    width: 6,
    margin: 3,
    borderRadius: 3,
  },
});

export const colors = StyleSheet.create({
  enabled: {
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
  },
  disabled: {
    backgroundColor: COLORS.MANATEE,
  },
});

export default styles;
