import PropTypes from 'prop-types';

export default {
  enabled: PropTypes.bool.isRequired,
};
