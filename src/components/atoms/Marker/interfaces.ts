/**
 * Interface
 */
export interface MarkerProps {
  enabled: boolean;
}
