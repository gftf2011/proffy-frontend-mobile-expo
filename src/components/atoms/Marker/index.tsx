import React from 'react';
import { View } from 'react-native';

/**
 * Styles
 */
import styles, { colors } from './styles';

/**
 * Interface
 */
import { MarkerProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const Marker: React.FC<MarkerProps> = ({ enabled }) => {
  return (
    <View style={[styles.marker, enabled ? colors.enabled : colors.disabled]} />
  );
};

export default Marker;

Marker.propTypes = PropTypes;
