import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Services
 */
import api, { defaultHeaders } from '../../../services';

/**
 * Atoms
 */
import Input from '../../atoms/Input';

/**
 * Molecules
 */
import Divisor from '../../molecules/Divisor';
import PasswordInput from '../../molecules/PasswordInput';

/**
 * Templates
 */
import ForgotPasswordTemplate from '../../templates/ForgotPasswordTemplate';

/**
 * Interfaces
 */
import { IPages } from './interfaces';
import { COLORS } from '../../atoms/Button/interfaces';
import {
  INPUT_BORDER_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_SIZE_TYPE,
} from '../../atoms/Input/interfaces';
import {
  PASSWORD_INPUT_BORDER_TYPE,
  PASSWORD_INPUT_CORNER_TYPE,
  PASSWORD_INPUT_SIZE_TYPE,
} from '../../molecules/PasswordInput/interfaces';

/**
 * Contexts
 */
import { useLoading } from '../../../context/LoadingContext';
import { useForgotPassword } from '../../../context/ForgotPasswordContext';

const ForgotPassword: React.FC = () => {
  const [isError, setIsError] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastText, setToastText] = useState('');
  const { setIsLoading } = useLoading();
  const {
    newPassword,
    setNewPassword,
    emailTo,
    setEmailTo,
    token,
    setToken,
    forgotPasswordIndex,
    setForgotPasswordIndex,
    setForgotPasswordPagesLength,
  } = useForgotPassword();

  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleSendEmail = async (): Promise<void> => {
    const dataBody = {
      email: emailTo,
    };

    try {
      setIsError(false);
      setIsLoading(true);
      await api(defaultHeaders()).post('/forgot_password', dataBody);
      setForgotPasswordIndex(1);
      setNewPassword('');
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const handleResetPassword = async (): Promise<void> => {
    const dataBody = {
      email: emailTo,
      token,
      newPassword,
    };

    try {
      setIsError(false);
      setIsLoading(true);
      await api(defaultHeaders()).post('/reset_password', dataBody);
      setForgotPasswordIndex(0);
      navigate('SuccessForgotPassword');
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  /**
   * Pages
   */
  const pages: IPages[] = [
    {
      color: COLORS.PRIMARY,
      title: 'Esqueceu sua senha?',
      subTitle: 'Não esquenta, vamos dar um jeito nisso.',
      allowOnPress: emailTo.length <= 0,
      buttonText: 'Enviar',
      element: (
        <View style={styles.inputContainer}>
          <Input
            inputBorder={INPUT_BORDER_TYPE.WITH_BORDER}
            inputCorner={INPUT_CORNER_TYPE.NO_CORNER}
            inputSize={INPUT_SIZE_TYPE.MEDIUM}
            onChangeText={setEmailTo}
            placeholder="Email"
          />
        </View>
      ),
      onPress: handleSendEmail,
    },
    {
      color: COLORS.SECONDARY,
      title: 'Sua nova senha!',
      subTitle:
        'Insira o token que enviamos para seu email junto com a nova senha.',
      allowOnPress: token.length <= 0 || newPassword.length <= 0,
      buttonText: 'Entrar',
      element: (
        <View style={styles.inputContainer}>
          <Input
            inputBorder={INPUT_BORDER_TYPE.WITH_BORDER}
            inputCorner={INPUT_CORNER_TYPE.NO_CORNER}
            inputSize={INPUT_SIZE_TYPE.MEDIUM}
            onChangeText={setToken}
            placeholder="Token"
          />
          <Divisor verticalGap={6} />
          <PasswordInput
            passwordInputBorder={PASSWORD_INPUT_BORDER_TYPE.WITH_BORDER}
            passwordInputCorner={PASSWORD_INPUT_CORNER_TYPE.NO_CORNER}
            passwordInputSize={PASSWORD_INPUT_SIZE_TYPE.MEDIUM}
            onChangeText={setNewPassword}
            placeholder="Nova senha"
          />
        </View>
      ),
      onPress: handleResetPassword,
    },
  ];

  /**
   * Effects
   */
  useEffect(() => {
    setForgotPasswordPagesLength(pages.length);
  }, [pages.length, setForgotPasswordPagesLength]);

  return (
    <ForgotPasswordTemplate
      buttonColor={pages[forgotPasswordIndex].color}
      buttonText={pages[forgotPasswordIndex].buttonText}
      title={pages[forgotPasswordIndex].title}
      subTitle={pages[forgotPasswordIndex].subTitle}
      isError={isError}
      showToast={isToastVisible}
      message={toastText}
      disabledButton={pages[forgotPasswordIndex].allowOnPress}
      onPress={() => {
        setIsToastVisible(!isToastVisible);
      }}
      onSubmit={pages[forgotPasswordIndex].onPress}
    >
      {pages[forgotPasswordIndex].element}
    </ForgotPasswordTemplate>
  );
};

export default ForgotPassword;
