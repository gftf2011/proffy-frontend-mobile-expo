import { ReactNode } from 'react';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export interface IPages {
  color: COLORS;
  title: string;
  subTitle: string;
  allowOnPress: boolean;
  buttonText: string;
  element: ReactNode;
  onPress: () => Promise<void>;
}
