import React from 'react';

/**
 * Templates
 */
import SuccessForgotPasswordTemplate from '../../templates/SuccessForgotPasswordTemplate';

const SuccessForgotPassword: React.FC = () => {
  return <SuccessForgotPasswordTemplate routeToGo="Login" />;
};

export default SuccessForgotPassword;
