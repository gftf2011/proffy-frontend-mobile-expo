import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Utils
 */
import { convertNumberToHourMinuteTimeFormat } from '../../../utils/StringUtils';

/**
 * Services
 */
import api, { headerWithAuthorization } from '../../../services';

/**
 * Templates
 */
import GiveClassesTemplate from '../../templates/GiveClassesTemplate';

/**
 * Contexts
 */
import { OfficeHour, useUser } from '../../../context/UserContext';
import { useClasses } from '../../../context/ClassesContext';
import { useLoading } from '../../../context/LoadingContext';

const GiveClasses: React.FC = () => {
  const {
    setIsTeacher,
    setOfficeHours,
    officeHours,
    whatsapp,
    bio,
    isTeacher,
  } = useUser();
  const { setSubject, subject, setCost, cost } = useClasses();
  const { setIsLoading } = useLoading();

  const [isError, setIsError] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastText, setToastText] = useState('');

  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleSubmitClasses = async (): Promise<void> => {
    try {
      setIsLoading(true);

      const dataBody = {
        isTeacher,
        cost,
        subject,
        whatsapp,
        bio,
        schedule: officeHours,
      };

      const token = await AsyncStorage.getItem('token');
      const response = await api(headerWithAuthorization(token as string)).post(
        '/classes',
        dataBody,
      );

      const { data } = response;

      const { user, classes, classSchedules } = data;

      await Promise.all([
        AsyncStorage.setItem('user', JSON.stringify(user)),
        AsyncStorage.setItem('classes', JSON.stringify(classes)),
        AsyncStorage.setItem('classSchedules', JSON.stringify(classSchedules)),
      ]);

      setIsTeacher(true);

      const schedules: OfficeHour[] = classSchedules;

      setOfficeHours(
        schedules.map(schedule => {
          const newSchedule: OfficeHour = {
            id: schedule.id,
            week_day: schedule.week_day,
            from: convertNumberToHourMinuteTimeFormat(schedule.from),
            to: convertNumberToHourMinuteTimeFormat(schedule.to),
            class_id: schedule.class_id,
          };
          return newSchedule;
        }),
      );

      setCost(classes.cost);
      setSubject(classes.subject);

      setIsError(false);
      setIsToastVisible(false);

      navigate('SuccessEditing');
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <GiveClassesTemplate
      submit={handleSubmitClasses}
      onPress={() => {
        setIsToastVisible(!isToastVisible);
      }}
      isError={isError}
      showToast={isToastVisible}
      message={toastText}
    />
  );
};

export default GiveClasses;
