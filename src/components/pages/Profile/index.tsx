import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Utils
 */
import { convertNumberToHourMinuteTimeFormat } from '../../../utils/StringUtils';

/**
 * Services
 */
import api, { headerWithAuthorization } from '../../../services';

/**
 * Templates
 */
import ProfileTemplate from '../../templates/ProfileTemplate';

/**
 * Contexts
 */
import { OfficeHour, useUser } from '../../../context/UserContext';
import { useClasses } from '../../../context/ClassesContext';
import { useLoading } from '../../../context/LoadingContext';
import { useAuth } from '../../../context/AuthContext';
import { useRemenberMe } from '../../../context/RemenberMeContext';

const Profile: React.FC = () => {
  const {
    setName,
    setLastName,
    setIsTeacher,
    setOfficeHours,
    setBio,
    setWhatsapp,
    setResumedName,
    name,
    lastName,
    officeHours,
    whatsapp,
    bio,
    isTeacher,
  } = useUser();
  const { setSubject, subject, setCost, cost } = useClasses();
  const { setIsLoading } = useLoading();
  const { emailToEdit, setEmail, setEmailToEdit } = useAuth();
  const { remenberMe } = useRemenberMe();

  const [isError, setIsError] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastText, setToastText] = useState('');

  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleSubmit = async (): Promise<void> => {
    try {
      setIsLoading(true);

      const classesData = isTeacher
        ? {
            cost,
            subject,
          }
        : null;

      const schedulesData = isTeacher ? officeHours : null;

      const userData = isTeacher
        ? {
            name,
            email: emailToEdit,
            last_name: lastName,
            isTeacher,
            whatsapp,
            bio,
          }
        : {
            name,
            email: emailToEdit,
            last_name: lastName,
            isTeacher,
          };

      const dataBody = {
        user: userData,
        classes: classesData,
        schedules: schedulesData,
      };

      const token = await AsyncStorage.getItem('token');
      const response = await api(headerWithAuthorization(token as string)).put(
        '/classes',
        dataBody,
      );

      const { data } = response;

      const { user, classes, classSchedules } = data;

      await Promise.all([
        AsyncStorage.setItem('user', JSON.stringify(user)),
        AsyncStorage.setItem('classes', JSON.stringify(classes)),
        AsyncStorage.setItem('classSchedules', JSON.stringify(classSchedules)),
      ]);

      if (isTeacher) {
        setBio(user.bio);
        setWhatsapp(user.whatsapp);

        setCost(classes.cost);
        setSubject(classes.subject);

        const schedules: OfficeHour[] = classSchedules;

        setOfficeHours(
          schedules.map(schedule => {
            const newSchedule: OfficeHour = {
              id: schedule.id,
              week_day: schedule.week_day,
              from: convertNumberToHourMinuteTimeFormat(schedule.from),
              to: convertNumberToHourMinuteTimeFormat(schedule.to),
              class_id: schedule.class_id,
            };
            return newSchedule;
          }),
        );
      }

      if (remenberMe) {
        setEmail(user.email);
        setEmailToEdit(user.email);
      }

      setName(user.name);
      setLastName(user.lastName);
      setResumedName(user.resumedName);
      setIsTeacher(user.isTeacher);

      setIsError(false);
      setIsToastVisible(false);

      navigate('Landing');
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <ProfileTemplate
      submit={handleSubmit}
      onPress={() => {
        setIsToastVisible(!isToastVisible);
      }}
      isError={isError}
      showToast={isToastVisible}
      message={toastText}
    />
  );
};

export default Profile;
