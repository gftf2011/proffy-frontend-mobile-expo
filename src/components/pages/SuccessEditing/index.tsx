import React from 'react';

/**
 * Templates
 */
import SuccessEditingTemplate from '../../templates/SuccessEditingTemplate';

const SuccessEditing: React.FC = () => {
  return <SuccessEditingTemplate routeToGo="Landing" />;
};

export default SuccessEditing;
