import React, { useEffect, ReactNode } from 'react';
import { Image } from 'react-native';

/**
 * Contexts
 */
import { useOnboardPage } from '../../../context/OnboardPageContext';

/**
 * Templates
 */
import OnboardTemplate from '../../templates/OnboardTemplate';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Images
 */
import STUDY_BACKGROUND from '../../../../assets/images/study-background.png';
import GIVE_CLASSES_BACKGROUND from '../../../../assets/images/give-classes-onboard-background.png';

/**
 * Interfaces
 */
interface IOnboardPage {
  color: string;
  page: string;
  image: ReactNode;
  text: string;
}

export const onboardPages: IOnboardPage[] = [
  {
    color: COLORS.MEDIUM_STATE_BLUE,
    page: '01',
    image: ((
      <Image
        resizeMode="contain"
        style={{ flex: 1 }}
        source={STUDY_BACKGROUND}
      />
    ) as unknown) as ReactNode,
    text: 'Encontre vários professores para ensinar você',
  },
  {
    color: COLORS.MALACHITE,
    page: '02',
    image: ((
      <Image
        resizeMode="contain"
        style={{ flex: 1 }}
        source={GIVE_CLASSES_BACKGROUND}
      />
    ) as unknown) as ReactNode,
    text: 'Ou dê aulas sobre o que você mais conhece',
  },
];

const Onboard: React.FC = () => {
  const { onboardIndex, setOnboardPagesLength } = useOnboardPage();

  /**
   * Effects
   */
  useEffect(() => {
    setOnboardPagesLength(onboardPages.length);
  }, []);

  return (
    <OnboardTemplate
      color={onboardPages[onboardIndex].color}
      text={onboardPages[onboardIndex].text}
      image={onboardPages[onboardIndex].image}
      pageNumber={onboardPages[onboardIndex].page}
    />
  );
};

export default Onboard;
