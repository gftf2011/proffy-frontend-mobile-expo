import React from 'react';

/**
 * Templates
 */
import SuccessSignInTemplate from '../../templates/SuccessSignInTemplate';

const SuccessSignIn: React.FC = () => {
  return <SuccessSignInTemplate routeToGo="Login" />;
};

export default SuccessSignIn;
