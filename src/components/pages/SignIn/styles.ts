import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  inputContainer: {
    overflow: 'hidden',
    width: '100%',
    backgroundColor: COLORS.CULTURED,
    borderColor: COLORS.MAGNOLIA,
    paddingVertical: 6,
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 12,
  },
});

export default styles;
