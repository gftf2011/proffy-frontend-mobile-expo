import { ReactNode } from 'react';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export interface IPages {
  color: COLORS;
  title: string;
  text: string;
  allowOnPress: boolean;
  element: ReactNode;
}
