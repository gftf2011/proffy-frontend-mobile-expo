import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

/**
 * Services
 */
import api, { defaultHeaders } from '../../../services';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Input from '../../atoms/Input';
import Select from '../../atoms/Select';

/**
 * Molecules
 */
import Divisor from '../../molecules/Divisor';
import PasswordInput from '../../molecules/PasswordInput';

/**
 * Templates
 */
import SignInTemplate from '../../templates/SignInTemplate';

/**
 * Hooks
 */
import useFetchCitiesByUfs from '../../../hooks/useFetchCitiesByUfs';

/**
 * Contants
 */
import { UF_LIST } from '../../../constants/data';

/**
 * Interfaces
 */
import { IPages } from './interfaces';
import { COLORS } from '../../atoms/Button/interfaces';
import {
  INPUT_BORDER_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_SIZE_TYPE,
} from '../../atoms/Input/interfaces';
import {
  PickerItem,
  SELECT_BORDER_TYPE,
  SELECT_CORNER_TYPE,
  SELECT_SIZE_TYPE,
} from '../../atoms/Select/interfaces';
import {
  PASSWORD_INPUT_BORDER_TYPE,
  PASSWORD_INPUT_CORNER_TYPE,
  PASSWORD_INPUT_SIZE_TYPE,
} from '../../molecules/PasswordInput/interfaces';

/**
 * Contexts
 */
import { useSignInPage } from '../../../context/SignInPageContext';
import { useAuth } from '../../../context/AuthContext';
import { useLoading } from '../../../context/LoadingContext';

const SignIn: React.FC = () => {
  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [ufSelected, setUfSelected] = useState(UF_LIST[0].value);
  const [citySelected, setCitySelected] = useState('');
  const { setIsLoading } = useLoading();
  const [isError, setIsError] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastText, setToastText] = useState('');
  const {
    signInIndex,
    signInPagesLength,
    setSignInIndex,
    setSignInPagesLength,
  } = useSignInPage();
  const { email, password, setEmail, setPassword } = useAuth();
  const { data, isValidating } = useFetchCitiesByUfs(ufSelected);

  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleUfSelected = (uf: string): void => {
    setUfSelected(uf);
  };

  const handleCitySelectOptionsByUfData = (): PickerItem[] => {
    if (!data) {
      return [{ label: 'Carregando', value: '' }];
    }
    return data;
  };

  const handleCitySelected = (city: string): void => {
    setCitySelected(city);
  };

  const handleClearAuth = (): void => {
    setEmail('');
    setPassword('');
  };

  const handleSubmitSignIn = async (): Promise<void> => {
    const dataBody = {
      city: citySelected,
      state: ufSelected,
      name,
      last_name: lastName,
      password,
      email,
      isTeacher: false,
    };

    try {
      setIsError(false);
      setIsLoading(true);
      await api(defaultHeaders()).post('/users', dataBody);
      setSignInIndex(0);
      handleClearAuth();
      navigate('SuccessSignIn');
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const signIn = async (index: number): Promise<void> => {
    signInIndex < signInPagesLength - 1
      ? setSignInIndex(++index)
      : await handleSubmitSignIn();
  };

  /**
   * Pages
   */
  const pages: IPages[] = [
    {
      color: COLORS.PRIMARY,
      title: '01. Quem é você?',
      text: 'Próximo',
      allowOnPress: name.length <= 0 || lastName.length <= 0,
      element: (
        <View style={styles.inputContainer}>
          <Input
            inputBorder={INPUT_BORDER_TYPE.WITH_BORDER}
            inputCorner={INPUT_CORNER_TYPE.NO_CORNER}
            inputSize={INPUT_SIZE_TYPE.MEDIUM}
            onChangeText={setName}
            placeholder="Nome"
          />
          <Divisor verticalGap={6} />
          <Input
            inputBorder={INPUT_BORDER_TYPE.WITH_BORDER}
            inputCorner={INPUT_CORNER_TYPE.NO_CORNER}
            inputSize={INPUT_SIZE_TYPE.MEDIUM}
            onChangeText={setLastName}
            placeholder="Sobrenome"
          />
        </View>
      ),
    },
    {
      color: COLORS.PRIMARY,
      title: '02. De onde você é?',
      text: 'Próximo',
      allowOnPress: isValidating,
      element: (
        <View style={styles.inputContainer}>
          <Select
            selectSize={SELECT_SIZE_TYPE.MEDIUM}
            selectBorder={SELECT_BORDER_TYPE.WITH_BORDER}
            selectCorner={SELECT_CORNER_TYPE.NO_CORNER}
            options={UF_LIST}
            selectedValue={ufSelected}
            onValueChange={item => {
              handleUfSelected(item as string);
            }}
          />
          <Divisor verticalGap={6} />
          <Select
            selectSize={SELECT_SIZE_TYPE.MEDIUM}
            selectBorder={SELECT_BORDER_TYPE.WITH_BORDER}
            selectCorner={SELECT_CORNER_TYPE.NO_CORNER}
            options={handleCitySelectOptionsByUfData()}
            selectedValue={citySelected}
            onValueChange={item => {
              handleCitySelected(item as string);
            }}
          />
        </View>
      ),
    },
    {
      color: COLORS.SECONDARY,
      title: '03. Email e Senha',
      text: 'Concluir Cadastro',
      allowOnPress: email.length <= 0 || password.length <= 0,
      element: (
        <View style={styles.inputContainer}>
          <Input
            inputBorder={INPUT_BORDER_TYPE.WITH_BORDER}
            inputCorner={INPUT_CORNER_TYPE.NO_CORNER}
            inputSize={INPUT_SIZE_TYPE.MEDIUM}
            onChangeText={setEmail}
            placeholder="Email"
          />
          <Divisor verticalGap={6} />
          <PasswordInput
            passwordInputBorder={PASSWORD_INPUT_BORDER_TYPE.WITH_BORDER}
            passwordInputCorner={PASSWORD_INPUT_CORNER_TYPE.NO_CORNER}
            passwordInputSize={PASSWORD_INPUT_SIZE_TYPE.MEDIUM}
            onChangeText={setPassword}
            placeholder="Senha"
          />
        </View>
      ),
    },
  ];

  /**
   * Effects
   */
  useEffect(() => {
    setSignInPagesLength(pages.length);
  }, []);

  useEffect(() => {
    setIsLoading(!data);
  }, [data, setIsLoading]);

  return (
    <SignInTemplate
      buttonText={pages[signInIndex].text}
      text={pages[signInIndex].title}
      buttonColor={pages[signInIndex].color}
      onPress={() => {
        setIsToastVisible(!isToastVisible);
      }}
      signIn={signIn}
      isError={isError}
      showToast={isToastVisible}
      message={toastText}
      disabledButton={pages[signInIndex].allowOnPress}
    >
      {pages[signInIndex].element}
    </SignInTemplate>
  );
};

export default SignIn;
