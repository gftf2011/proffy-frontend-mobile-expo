import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Utils
 */
import { convertNumberToHourMinuteTimeFormat } from '../../../utils/StringUtils';

/**
 * Services
 */
import api, { defaultHeaders } from '../../../services';

/**
 * Contexts
 */
import { useClasses } from '../../../context/ClassesContext';
import { useAuth } from '../../../context/AuthContext';
import { OfficeHour, useUser } from '../../../context/UserContext';
import { useLoading } from '../../../context/LoadingContext';

/**
 * Templates
 */
import LoginTemplate from '../../templates/LoginTemplate';

const Login: React.FC = () => {
  const { email, password } = useAuth();
  const {
    setName,
    setLastName,
    setResumedName,
    setIsTeacher,
    setBio,
    setWhatsapp,
    setOfficeHours,
  } = useUser();
  const { setCost, setSubject } = useClasses();
  const { setIsLoading } = useLoading();

  const [isError, setIsError] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastText, setToastText] = useState('');

  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleSubmit = async (): Promise<void> => {
    const dataBody = {
      password,
      email,
    };

    try {
      setIsLoading(true);

      const response = await api(defaultHeaders()).post('/sessions', dataBody);
      const { data } = response;

      const { user, token, classes, classSchedules } = data;

      await Promise.all([
        AsyncStorage.setItem('user', JSON.stringify(user)),
        AsyncStorage.setItem('classes', JSON.stringify(classes)),
        AsyncStorage.setItem('classSchedules', JSON.stringify(classSchedules)),
        AsyncStorage.setItem('token', `Bearer ${token}`),
      ]);

      setName(user.name);
      setLastName(user.lastName);
      setResumedName(user.resumedName);
      setBio(user.bio ? user.bio : '');
      setWhatsapp(user.whatsapp ? user.whatsapp : '');
      setIsTeacher(user.isTeacher);

      const schedules: OfficeHour[] = classSchedules;

      setOfficeHours(
        schedules.map(schedule => {
          const newSchedule: OfficeHour = {
            id: schedule.id,
            week_day: schedule.week_day,
            from: convertNumberToHourMinuteTimeFormat(schedule.from),
            to: convertNumberToHourMinuteTimeFormat(schedule.to),
            class_id: schedule.class_id,
          };
          return newSchedule;
        }),
      );

      setCost(classes.cost ? classes.cost : 0);
      setSubject(classes.subject ? classes.subject : '');

      setIsError(false);
      setIsToastVisible(false);

      navigate('Landing');
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <LoginTemplate
      onPress={() => {
        setIsToastVisible(!isToastVisible);
      }}
      onSubmit={handleSubmit}
      isError={isError}
      showToast={isToastVisible}
      message={toastText}
    />
  );
};

export default Login;
