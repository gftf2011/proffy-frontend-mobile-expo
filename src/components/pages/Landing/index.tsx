import React, { useEffect, useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Services
 */
import api, { defaultHeaders } from '../../../services';

/**
 * Templates
 */
import LandingTemplate from '../../templates/LandingTemplate';

/**
 * Contexts
 */
import { useUser } from '../../../context/UserContext';

const Landing: React.FC = () => {
  const [totalConections, setTotalConections] = useState(0);
  const { setPicture } = useUser();

  /**
   * Handlers
   */
  const handleTotalConnections = async () => {
    const response = await api(defaultHeaders()).get('connections');
    const { total } = response.data;
    setTotalConections(total);
  };

  useEffect(() => {
    const fetch = async (): Promise<void> => {
      const picture = await AsyncStorage.getItem('picture');
      if (picture) {
        setPicture(picture);
      }
    };
    fetch();
  }, [setPicture]);

  useFocusEffect(() => {
    handleTotalConnections();
  });

  return <LandingTemplate totalConections={totalConections} />;
};

export default Landing;
