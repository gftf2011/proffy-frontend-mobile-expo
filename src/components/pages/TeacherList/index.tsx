import React, { useState } from 'react';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Services
 */
import api, { headerWithAuthorization } from '../../../services';

/**
 * Templates
 */
import TeacherListTemplate from '../../templates/TeacherListTemplate';

/**
 * Contexts
 */
import { useLoading } from '../../../context/LoadingContext';
import { useTeacherList, Teacher } from '../../../context/TeacherListContext';
import {
  FavoriteTeacher,
  useFavoriteTeacherList,
} from '../../../context/FavoriteTeacherListContext';

const TeacherList: React.FC = () => {
  const { setIsLoading } = useLoading();
  const { setTeachers, subject, time, weekDay } = useTeacherList();
  const { setTeachers: setFavoriteTeachers } = useFavoriteTeacherList();
  const [isError, setIsError] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [toastText, setToastText] = useState('');

  /**
   * Handlers
   */
  const handleSetTeacherAsUnfavorite = async (
    teacherId: number,
  ): Promise<void> => {
    try {
      setIsError(false);
      setIsLoading(true);

      const token = await AsyncStorage.getItem('token');

      const [_, favorites, teachers] = await Promise.all([
        api(headerWithAuthorization(token as string)).delete(
          `/favorites/${teacherId}`,
        ),
        AsyncStorage.getItem('favorites'),
        AsyncStorage.getItem('teacher'),
      ]);

      if (favorites && teachers) {
        const parsedFavorites: Teacher[] = JSON.parse(favorites);
        const parsedTeachers: Teacher[] = JSON.parse(teachers);

        const filteredFavorites = parsedFavorites.filter(
          favorite => favorite.id === teacherId,
        );
        const mappedTeachers = parsedTeachers.map(parsedTeacher => {
          if (parsedTeacher.id === teacherId) {
            return {
              ...parsedTeacher,
              isFavorite: false,
            };
          }
          return parsedTeacher;
        });

        const updatedFavorites = filteredFavorites || [];
        const updatedTeachers = mappedTeachers || [];

        await Promise.all([
          AsyncStorage.setItem('teacher', JSON.stringify(updatedTeachers)),
          AsyncStorage.setItem('favorites', JSON.stringify(updatedFavorites)),
        ]);

        setFavoriteTeachers(updatedFavorites);
        setTeachers(updatedTeachers);
      }
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSetTeacherAsFavorite = async (
    teacherId: number,
  ): Promise<void> => {
    try {
      setIsError(false);
      setIsLoading(true);

      const token = await AsyncStorage.getItem('token');

      const [response, teachers] = await Promise.all([
        api(headerWithAuthorization(token as string)).patch('/classes', {
          teacher_id: teacherId,
        }),
        AsyncStorage.getItem('teacher'),
      ]);

      const { data } = response;
      const favorites: FavoriteTeacher[] = data;

      if (teachers) {
        const parsedTeachers: Teacher[] = JSON.parse(teachers);
        const updatedTeachers = parsedTeachers.map(parsedTeacher => {
          const { id } = parsedTeacher;
          return {
            ...parsedTeacher,
            isFavorite: favorites.some(favorite => favorite.teacher_id === id),
          };
        });
        const filteredTeachers = updatedTeachers.filter(
          updatedTeacher => updatedTeacher.isFavorite === false,
        );
        const favoriteTeachers = filteredTeachers || [];

        await Promise.all([
          AsyncStorage.setItem('teacher', JSON.stringify(updatedTeachers)),
          AsyncStorage.setItem('favorites', JSON.stringify(favoriteTeachers)),
        ]);

        setFavoriteTeachers(favoriteTeachers);
        setTeachers(updatedTeachers);
      }
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSearch = async (): Promise<void> => {
    try {
      setIsError(false);
      setIsLoading(true);

      const token = await AsyncStorage.getItem('token');

      const response = await api(headerWithAuthorization(token as string)).get(
        `/classes?week_day=${weekDay}&subject=${encodeURIComponent(
          subject,
        )}&time=${encodeURIComponent(time)}`,
      );
      const { data } = response;
      const teachers: Teacher[] = data;

      await AsyncStorage.setItem('teacher', JSON.stringify(teachers));

      setTeachers(teachers);
    } catch (err) {
      setIsError(true);
      setIsToastVisible(true);
      setToastText(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <TeacherListTemplate
      onPress={() => {
        setIsToastVisible(!isToastVisible);
      }}
      onSearch={handleSearch}
      onFavorite={handleSetTeacherAsFavorite}
      onUnfavorite={handleSetTeacherAsUnfavorite}
      isError={isError}
      showToast={isToastVisible}
      message={toastText}
    />
  );
};

export default TeacherList;
