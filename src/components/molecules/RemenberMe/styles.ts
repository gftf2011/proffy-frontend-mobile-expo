import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  remenberMeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  remenberMeText: {
    fontSize: 12,
    lineHeight: 24,
    color: COLORS.MANATEE,
    fontFamily: 'Poppins_400Regular',
    marginLeft: 6,
  },
});

export default styles;
