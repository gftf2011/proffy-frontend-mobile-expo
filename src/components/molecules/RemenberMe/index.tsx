import React from 'react';
import { View, TouchableWithoutFeedback, Text } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Checkbox from '../../atoms/Checkbox';

/**
 * Context
 */
import { useRemenberMe } from '../../../context/RemenberMeContext';

const RemenberMe: React.FC = () => {
  const { remenberMe, setRemenberMe } = useRemenberMe();
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        setRemenberMe(!remenberMe);
      }}
    >
      <View style={styles.remenberMeContainer}>
        <Checkbox disabled value={remenberMe} />
        <Text style={styles.remenberMeText}>Lembrar-me</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default RemenberMe;
