import React from 'react';
import { TouchableOpacity, TouchableOpacityProps, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';

/**
 * Images
 */
import BACK_ICON from '../../../../assets/images/icons/back.png';

/**
 * Styles
 */
import styles from './styles';

const GoBackHeaderButton: React.FC<TouchableOpacityProps> = () => {
  /**
   * Navigation
   */
  const { goBack } = useNavigation();

  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => {
        goBack();
      }}
    >
      <Image source={BACK_ICON} />
    </TouchableOpacity>
  );
};

export default GoBackHeaderButton;
