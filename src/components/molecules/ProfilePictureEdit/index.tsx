import React from 'react';
import { View, Image, Text, Platform } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { ImageInfo } from 'expo-image-picker/build/ImagePicker.types';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import { ButtonRounded } from '../../atoms/Button';
import Icon from '../../atoms/Icon';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Interfaces
 */
import { IconType } from '../../atoms/Icon/interfaces';
import { COLORS as BUTTON_COLLORS } from '../../atoms/Button/interfaces';

/**
 * Contexts
 */
import { useUser } from '../../../context/UserContext';
import { useClasses } from '../../../context/ClassesContext';

const ProfilePictureEdit: React.FC = () => {
  const { resumedName, picture, setPicture, isTeacher } = useUser();
  const { subject } = useClasses();

  /**
   * Handlers
   */
  const handleImageSelect = async (): Promise<void> => {
    if (Platform.OS !== 'web') {
      const { granted } = await ImagePicker.requestCameraRollPermissionsAsync();

      if (!granted) {
        return;
      }

      const photo = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });

      if (photo.cancelled) {
        return;
      }

      const photoPicked = photo as ImageInfo;

      await AsyncStorage.setItem('picture', photoPicked.uri);

      setPicture(photoPicked.uri);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        {picture !== '' ? (
          <Image style={styles.profileImage} source={{ uri: picture }} />
        ) : (
          <View style={styles.noImage}>
            <Icon
              type={IconType.FEATHER}
              name="camera-off"
              color={COLORS.MANATEE}
              size={72}
            />
          </View>
        )}
        <View style={styles.pickImageButtonWrapper}>
          <ButtonRounded
            onPress={handleImageSelect}
            buttonColor={BUTTON_COLLORS.SECONDARY}
          >
            <Icon
              type={IconType.FEATHER}
              name="camera"
              color={COLORS.GHOST_WHITE}
              size={24}
            />
          </ButtonRounded>
        </View>
      </View>
      <Text style={styles.name}>{resumedName}</Text>
      {/* {isTeacher ? <Text style={styles.subject}>{subject}</Text> : <></>} */}
    </View>
  );
};

export default ProfilePictureEdit;
