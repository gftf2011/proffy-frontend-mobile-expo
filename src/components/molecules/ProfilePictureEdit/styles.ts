import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    width: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    height: 140,
    width: 140,
    position: 'relative',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  noImage: {
    height: 140,
    width: 140,
    borderRadius: 70,
    backgroundColor: COLORS.MAJORELLE_BLUE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileImage: {
    height: 140,
    width: 140,
    borderRadius: 70,
  },
  pickImageButtonWrapper: {
    height: 40,
    width: 40,
    position: 'absolute',
  },
  name: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 24,
    lineHeight: 25,
    marginTop: 24,
    color: COLORS.WHITE,
  },
  subject: {
    marginTop: 6,
    fontFamily: 'Poppins_400Regular',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.LAVENDER_BLUE,
  },
});

export default styles;
