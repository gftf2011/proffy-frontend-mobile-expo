import React from 'react';
import { View, Animated } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Styles
 */
import styles from './styles';

/**
 * Animations
 */
import animation from './animation';

/**
 * Atoms
 */
import Icon from '../../atoms/Icon';

/**
 * Interfaces
 */
import { IconType } from '../../atoms/Icon/interfaces';

/**
 * Contexts
 */
import { useLoading } from '../../../context/LoadingContext';

const Loader: React.FC = () => {
  const { isLoading } = useLoading();
  return (
    <>
      {isLoading && (
        <View style={styles.loaderContainer}>
          <Animated.View
            style={[styles.loader, { transform: [{ rotate: animation() }] }]}
          >
            <Icon
              type={IconType.FONT_AWESOME}
              color={COLORS.MEDIUM_STATE_BLUE}
              size={32}
              name="spinner"
            />
          </Animated.View>
        </View>
      )}
    </>
  );
};

export default Loader;
