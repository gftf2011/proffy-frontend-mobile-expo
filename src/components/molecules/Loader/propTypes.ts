import PropTypes from 'prop-types';

export default {
  isLoading: PropTypes.bool.isRequired,
};
