import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  loaderContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
    position: 'absolute',
    backgroundColor: COLORS.BLACK,
    opacity: 0.75,
    zIndex: 10,
  },
  loader: {
    height: 32,
    width: 32,
  },
});

export default styles;
