import React from 'react';
import { TouchableOpacity } from 'react-native';
import { useNavigation, StackActions } from '@react-navigation/native';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Icon from '../../atoms/Icon';

/**
 * Interfaces
 */
import { LogOffButtonProps } from './interfaces';
import { IconType } from '../../atoms/Icon/interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const LogOffButton: React.FC<LogOffButtonProps> = ({ route }) => {
  /**
   * Navigation
   */
  const { dispatch } = useNavigation();
  const { replace } = StackActions;

  /**
   * Handlers
   */
  const handleNavigation = async (): Promise<void> => {
    await AsyncStorage.setItem('token', '');
    dispatch(replace(route));
  };

  return (
    <TouchableOpacity style={styles.button} onPress={handleNavigation}>
      <Icon
        size={24}
        type={IconType.FEATHER}
        name="power"
        color={COLORS.LAVENDER_BLUE}
      />
    </TouchableOpacity>
  );
};

export default LogOffButton;

LogOffButton.propTypes = PropTypes;
