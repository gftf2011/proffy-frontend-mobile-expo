import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  button: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    backgroundColor: COLORS.MAJORELLE_BLUE,
  },
});

export default styles;
