import PropTypes from 'prop-types';

export default {
  route: PropTypes.string.isRequired,
};
