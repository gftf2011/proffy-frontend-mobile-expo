import React from 'react';
import { View, Image, Text } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Icon from '../../atoms/Icon';

/**
 * Interfaces
 */
import { ProfileSubjectProps } from './interfaces';
import { IconType } from '../../atoms/Icon/interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const ProfileSubject: React.FC<ProfileSubjectProps> = ({
  picture = '',
  resumedName = '',
  subject = '',
}) => {
  return (
    <View style={styles.profileContainer}>
      {picture !== '' ? (
        <Image style={styles.profileImage} source={{ uri: picture }} />
      ) : (
        <View style={styles.profileIcon}>
          <Icon
            type={IconType.FEATHER}
            name="camera"
            color={COLORS.MANATEE}
            size={24}
          />
        </View>
      )}
      <View style={styles.infoContainer}>
        <Text style={styles.nameText}>{resumedName}</Text>
        <Text style={styles.subjectText}>{subject}</Text>
      </View>
    </View>
  );
};

export default ProfileSubject;

ProfileSubject.propTypes = PropTypes;
