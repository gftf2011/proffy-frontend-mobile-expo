import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  profileContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileIcon: {
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.MAJORELLE_BLUE,
  },
  profileImage: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  infoContainer: {
    height: 50,
    marginLeft: 12,
  },
  nameText: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 20,
    lineHeight: 25,
    color: COLORS.RUSSIAN_VIOLET,
  },
  subjectText: {
    marginTop: 6,
    fontFamily: 'Poppins_400Regular',
    fontSize: 12,
    lineHeight: 20,
    color: COLORS.DARK_BLUE_GRAY,
  },
});

export default styles;
