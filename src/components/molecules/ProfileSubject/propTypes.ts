import PropTypes from 'prop-types';

export default {
  picture: PropTypes.string,
  resumedName: PropTypes.string,
  subject: PropTypes.string,
};
