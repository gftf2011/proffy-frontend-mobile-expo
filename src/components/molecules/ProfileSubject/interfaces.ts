export interface ProfileSubjectProps {
  picture?: string;
  resumedName?: string;
  subject?: string;
}
