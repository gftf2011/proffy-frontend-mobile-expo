import PropTypes from 'prop-types';

export default {
  index: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  showDeleteButton: PropTypes.bool,
};
