import React, { useState, useEffect } from 'react';
import { Text, View, Platform } from 'react-native';
import { format } from 'date-fns';

/**
 * Constants
 */
import { TIME_OPTIONS } from '../../../constants/data';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import { ButtonWrapper } from '../../atoms/Button';
import TimePicker from '../../atoms/TimePicker';
import Select from '../../atoms/Select';

/**
 * Molecules
 */
import Divisor from '../Divisor';

/**
 * Interfaces
 */
import { FormTimeSelectorsItemProps } from './interfaces';
import { COLORS } from '../../atoms/Button/interfaces';
import {
  TIMEPICKER_BORDER_TYPE,
  TIMEPICKER_CORNER_TYPE,
  TIMEPICKER_SIZE_TYPE,
} from '../../atoms/TimePicker/interfaces';
import {
  SELECT_BORDER_TYPE,
  SELECT_CORNER_TYPE,
  SELECT_SIZE_TYPE,
} from '../../atoms/Select/interfaces';

/**
 * Contexts
 */
import { useUser, OfficeHour } from '../../../context/UserContext';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const FormTimeSelectorsItem: React.FC<FormTimeSelectorsItemProps> = ({
  index,
  position,
}) => {
  const [showFrom, setShowFrom] = useState(false);
  const [showTo, setShowTo] = useState(false);
  const [beginningTime, setBeginningTime] = useState(new Date());
  const [endingTime, setEndingTime] = useState(new Date());

  const { isTeacher, setOfficeHours, officeHours } = useUser();

  /**
   * Effects
   */
  useEffect(() => {
    if (isTeacher) {
      const [beginningHours, beginningMinutes] = officeHours[position].from
        .toString()
        .split(':');
      const [endingHours, endingMinutes] = officeHours[position].to
        .toString()
        .split(':');

      const beginning = new Date();
      const ending = new Date();

      beginning.setHours(parseInt(beginningHours, 10));
      beginning.setMinutes(parseInt(beginningMinutes, 10));

      ending.setHours(parseInt(endingHours, 10));
      ending.setMinutes(parseInt(endingMinutes, 10));

      setBeginningTime(beginning);
      setEndingTime(ending);
    }
  }, []);

  /**
   * Handlers
   */
  const handleBeginTime = (_event: Event, date: Date): void => {
    setShowFrom(Platform.OS === 'ios');
    const todayTime = format(date, 'HH:mm');
    const time = officeHours.map(officeHour => {
      if (officeHour.id === index) {
        const { id, week_day, to } = officeHour;
        const data: OfficeHour = {
          id,
          week_day,
          to,
          from: todayTime,
        };
        return data;
      }
      return officeHour;
    });
    setBeginningTime(date);
    setOfficeHours(time);
  };

  const handleEndTime = (_event: Event, date: Date): void => {
    setShowTo(Platform.OS === 'ios');
    const todayTime = format(date, 'HH:mm');
    const time = officeHours.map(officeHour => {
      if (officeHour.id === index) {
        const { id, week_day, from } = officeHour;
        const data: OfficeHour = {
          id,
          week_day,
          to: todayTime,
          from,
        };
        return data;
      }
      return officeHour;
    });
    setEndingTime(date);
    setOfficeHours(time);
  };

  const handleWeekDay = (
    itemValue: string | number,
    _itemIndex: number,
  ): void => {
    const time = officeHours.map(officeHour => {
      if (officeHour.id === index) {
        const { id, from, to } = officeHour;
        const data: OfficeHour = {
          id,
          week_day: itemValue,
          to,
          from,
        };
        return data;
      }
      return officeHour;
    });
    setOfficeHours(time);
  };

  const handleDeleteItem = (): void => {
    const time = officeHours.filter(officeHour => officeHour.id !== index);
    setOfficeHours(time);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Dia da semana</Text>
      <Select
        selectBorder={SELECT_BORDER_TYPE.NO_BORDER}
        selectCorner={SELECT_CORNER_TYPE.WITH_CORNER}
        selectSize={SELECT_SIZE_TYPE.MEDIUM}
        options={TIME_OPTIONS}
        selectedValue={officeHours[position].week_day}
        onValueChange={handleWeekDay}
      />
      <View style={styles.timeWrapper}>
        <View style={styles.timeInnerWrapper}>
          <Text style={styles.label}>Das</Text>
          <TimePicker
            timePickerBorder={TIMEPICKER_BORDER_TYPE.NO_BORDER}
            timePickerCorner={TIMEPICKER_CORNER_TYPE.WITH_CORNER}
            timePickerSize={TIMEPICKER_SIZE_TYPE.MEDIUM}
            mode="time"
            is24Hour
            value={endingTime}
            text={format(beginningTime, 'HH:mm')}
            show={showFrom}
            onPress={() => {
              setShowFrom(true);
            }}
            onChange={(event: any, date: any) => {
              handleBeginTime(event, date);
            }}
          />
        </View>
        <View style={styles.spacing} />
        <View style={styles.timeInnerWrapper}>
          <Text style={styles.label}>Até</Text>
          <TimePicker
            timePickerBorder={TIMEPICKER_BORDER_TYPE.NO_BORDER}
            timePickerCorner={TIMEPICKER_CORNER_TYPE.WITH_CORNER}
            timePickerSize={TIMEPICKER_SIZE_TYPE.MEDIUM}
            mode="time"
            is24Hour
            value={beginningTime}
            text={format(endingTime, 'HH:mm')}
            show={showTo}
            onPress={() => {
              setShowTo(true);
            }}
            onChange={(event: any, date: any) => {
              handleEndTime(event, date);
            }}
          />
        </View>
      </View>
      {officeHours.length > 1 && (
        <View style={styles.excludeItemContainer}>
          <View style={styles.divisorWrapper}>
            <Divisor />
          </View>
          <View style={styles.spacing} />
          <ButtonWrapper
            onPress={handleDeleteItem}
            buttonColor={COLORS.TRANSPARENT}
          >
            <Text style={styles.excludeItemText}>Excluir horário</Text>
          </ButtonWrapper>
          <View style={styles.spacing} />
          <View style={styles.divisorWrapper}>
            <Divisor />
          </View>
        </View>
      )}
    </View>
  );
};

export default FormTimeSelectorsItem;

FormTimeSelectorsItem.propTypes = PropTypes;
