export interface FormTimeSelectorsItemProps {
  index: number;
  position: number;
  showDeleteButton?: boolean;
}
