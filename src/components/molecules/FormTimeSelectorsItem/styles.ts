import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  label: {
    marginVertical: 4,
    fontSize: 12,
    lineHeight: 22,
    fontFamily: 'Poppins_400Regular',
    color: COLORS.MANATEE,
  },
  timeWrapper: {
    height: 82,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  timeInnerWrapper: {
    flex: 1,
  },
  spacing: {
    width: 16,
    height: '100%',
  },
  excludeItemContainer: {
    marginTop: 16,
    height: 32,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  divisorWrapper: {
    flex: 1,
    flexWrap: 'wrap',
  },
  excludeItemText: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 12,
    lineHeight: 20,
    color: COLORS.IMPERIAL_RED,
  },
});

export default styles;
