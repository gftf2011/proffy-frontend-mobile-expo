import React, { useEffect, useState } from 'react';
import { View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Marker from '../../atoms/Marker';

/**
 * Interfaces
 */
import { MarkerListProps } from './interfaces';

/**
 * PropTypes
 */
import PropTypes from './propTypes';

const MarkerList: React.FC<MarkerListProps> = ({ index, size }) => {
  const [pages, setPages] = useState<JSX.Element[]>([]);

  /**
   * Effects
   */
  useEffect(() => {
    const pagesVector = [];

    for (let i = 0; i < size; i++) {
      pagesVector.push(<Marker key={i} enabled={i === index} />);
    }
    setPages(pagesVector);
  }, [index, size]);

  return (
    <View style={styles.list}>
      {pages.map((element: JSX.Element) => element)}
    </View>
  );
};

export default MarkerList;

MarkerList.propTypes = PropTypes;
