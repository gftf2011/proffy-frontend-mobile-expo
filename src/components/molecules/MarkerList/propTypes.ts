import PropTypes from 'prop-types';

export default {
  index: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
};
