import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  profileContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  profileIcon: {
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.MAJORELLE_BLUE,
  },
  profileImage: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  profileText: {
    fontSize: 24,
    marginTop: 4,
    textAlign: 'center',
    paddingHorizontal: 8,
    color: COLORS.LAVENDER_BLUE,
  },
});

export default styles;
