import React from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
// import * as ImagePicker from 'expo-image-picker';
// import { ImageInfo } from 'expo-image-picker/build/ImagePicker.types';

/**
 * Storage
 */
// import AsyncStorage from '@react-native-community/async-storage';

/**
 * Services
 */
// import api, { headersFile } from '../../../services';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Icon from '../../atoms/Icon';

/**
 * Props
 */
import PropTypes from './propTypes';

/**
 * Interfaces
 */
import { IconType } from '../../atoms/Icon/interfaces';
import { ProfilePictureProps } from './interfaces';

/**
 * Contexts
 */
import { useUser } from '../../../context/UserContext';

const ProfilePicture: React.FC<ProfilePictureProps> = ({
  name = 'NOME COMPLETO',
}) => {
  // const { picture, setPicture } = useUser();

  const { picture } = useUser();
  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleNavigation = (): void => {
    navigate('Profile');
  };
  // const handleImageSelect = async (): Promise<void> => {
  //   if (Platform.OS !== 'web') {
  //     const { granted } = await ImagePicker.requestCameraRollPermissionsAsync();

  //     if (!granted) {
  //       return;
  //     }

  //     const photo = await ImagePicker.launchImageLibraryAsync({
  //       allowsEditing: true,
  //       aspect: [4, 3],
  //       quality: 1,
  //     });

  //     if (photo.cancelled) {
  //       return;
  //     }

  //     const photoPicked = photo as ImageInfo;

  //     // const file = await fetch(photoPicked.uri);
  //     // const fileBlob = await file.blob();

  //     await AsyncStorage.setItem('picture', photoPicked.uri);

  //     // const data = new FormData();
  //     // data.append(
  //     //   'avatar',
  //     //   fileBlob,
  //     //   photoPicked.uri.substr(photoPicked.uri.lastIndexOf('/') + 1),
  //     // );

  //     // const token = await AsyncStorage.getItem('token');

  //     // const d = await api(headersFile(token as string)).post('/files', data);

  //     // console.log(d);

  //     setPicture(photoPicked.uri);
  //   }
  // };

  return (
    <TouchableOpacity
      style={styles.profileContainer}
      onPress={handleNavigation}
    >
      {picture !== '' ? (
        <Image style={styles.profileImage} source={{ uri: picture }} />
      ) : (
        <View style={styles.profileIcon}>
          <Icon
            type={IconType.FEATHER}
            name="camera"
            color={COLORS.MANATEE}
            size={24}
          />
        </View>
      )}
      <Text style={styles.profileText}>{name}</Text>
    </TouchableOpacity>
  );
};

export default ProfilePicture;

ProfilePicture.propTypes = PropTypes;
