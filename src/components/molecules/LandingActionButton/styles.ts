import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  button: {
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    padding: 30,
    width: '47%',
    height: 150,
    borderRadius: 8,
  },
  text: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 20,
    lineHeight: 22,
    color: COLORS.WHITE,
  },
});

export const colors = StyleSheet.create({
  primary: {
    backgroundColor: COLORS.PLUMP_PURPLE,
  },
  secondary: {
    backgroundColor: COLORS.MALACHITE,
  },
  error: {
    backgroundColor: COLORS.IMPERIAL_RED,
  },
});

export default styles;
