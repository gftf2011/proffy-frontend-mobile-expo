import { ViewStyle } from 'react-native';

import { COLORS } from './interfaces';

import { colors } from './styles';

export const handleButtonColor = (buttonColors?: COLORS): ViewStyle => {
  const { PRIMARY, SECONDARY, ERROR } = COLORS;
  switch (buttonColors) {
    case PRIMARY:
      return colors.primary;
    case SECONDARY:
      return colors.secondary;
    case ERROR:
      return colors.error;
    default:
      return colors.primary;
  }
};
