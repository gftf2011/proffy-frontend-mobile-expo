import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Themes
 */
import { handleButtonColor } from './themes';

/**
 * Interfaces
 */
import { LoadingActionButtonProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const LandingActionButton: React.FC<LoadingActionButtonProps> = ({
  route,
  text,
  buttonColor,
  children,
}) => {
  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleNavigation = (): void => {
    navigate(route);
  };

  return (
    <TouchableOpacity
      onPress={handleNavigation}
      style={[styles.button, handleButtonColor(buttonColor)]}
    >
      {children}
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

export default LandingActionButton;

LandingActionButton.propTypes = PropTypes;
