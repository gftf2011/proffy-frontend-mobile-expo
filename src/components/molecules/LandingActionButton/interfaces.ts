import { ReactNode } from 'react';
import { TouchableOpacityProps } from 'react-native';

/**
 * Enums
 */
export enum COLORS {
  PRIMARY = 0,
  SECONDARY = 1,
  ERROR = 2,
}

export interface LoadingActionButtonProps extends TouchableOpacityProps {
  text: string;
  route: string;
  buttonColor?: COLORS;
  children: ReactNode;
}
