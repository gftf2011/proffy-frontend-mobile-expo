import PropTypes from 'prop-types';

import { COLORS } from './interfaces';

export default {
  buttonColor: PropTypes.oneOf([
    COLORS.PRIMARY,
    COLORS.SECONDARY,
    COLORS.ERROR,
  ]),
  route: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};
