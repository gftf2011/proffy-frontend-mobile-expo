import React from 'react';
import { Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';

/**
 * Icons
 */
import BACK from '../../../../assets/images/icons/back.png';

/**
 * Atoms
 */
import { ButtonWrapper } from '../../atoms/Button';

/**
 * Contexts
 */
import { useSignInPage } from '../../../context/SignInPageContext';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

const GoBackSignInButton: React.FC = () => {
  const { signInIndex, setSignInIndex } = useSignInPage();

  /**
   * Navigation
   */
  const { goBack } = useNavigation();

  /**
   * Handlers
   */
  const handleNavigation = (): void => {
    goBack();
  };

  const handlePreviousPage = (value: number): void => {
    setSignInIndex(--value);
  };

  return (
    <ButtonWrapper
      buttonColor={COLORS.TRANSPARENT}
      onPress={() => {
        signInIndex > 0 ? handlePreviousPage(signInIndex) : handleNavigation();
      }}
    >
      <Image source={BACK} />
    </ButtonWrapper>
  );
};

export default GoBackSignInButton;
