import PropTypes from 'prop-types';

export default {
  day: PropTypes.string.isRequired,
  start: PropTypes.number,
  end: PropTypes.number,
  disabled: PropTypes.bool,
};
