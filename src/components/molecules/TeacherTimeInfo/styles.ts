import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    marginVertical: 4,
    paddingHorizontal: 15,
    height: 40,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: COLORS.MAGNOLIA,
    backgroundColor: COLORS.CULTURED,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  day: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 14,
    lineHeight: 18,
    color: COLORS.DARK_BLUE_GRAY,
  },
  disabledDay: {
    opacity: 0.5,
  },
  time: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 14,
    lineHeight: 18,
    color: COLORS.DARK_BLUE_GRAY,
  },
});

export default styles;
