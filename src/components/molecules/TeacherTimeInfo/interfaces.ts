export interface TeacherTimeInfoProps {
  start?: number;
  end?: number;
  disabled?: boolean;
  day: string;
}
