import React from 'react';
import { View, Text } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Utils
 */
import { convertNumberToHourMinuteTimeFormat } from '../../../utils/StringUtils';

/**
 * Interfaces
 */
import { TeacherTimeInfoProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const TeacherTimeInfo: React.FC<TeacherTimeInfoProps> = ({
  day,
  start = 0,
  end = 0,
  disabled = false,
}) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.day, disabled && styles.disabledDay]}>{day}</Text>
      <Text style={styles.time}>
        {!disabled &&
          `${convertNumberToHourMinuteTimeFormat(
            start,
          )}h - ${convertNumberToHourMinuteTimeFormat(end)}h`}
      </Text>
    </View>
  );
};

export default TeacherTimeInfo;

TeacherTimeInfo.propTypes = PropTypes;
