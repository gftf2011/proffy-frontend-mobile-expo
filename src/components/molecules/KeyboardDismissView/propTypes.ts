import PropTypes from 'prop-types';
import { BEHAVIOR_TYPE } from './interfaces';

export default {
  height: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  behaviorType: PropTypes.oneOf([
    BEHAVIOR_TYPE.HEIGHT,
    BEHAVIOR_TYPE.PADDING,
    BEHAVIOR_TYPE.POSITION,
  ]),
};
