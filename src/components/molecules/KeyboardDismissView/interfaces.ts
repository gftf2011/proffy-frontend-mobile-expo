import { ReactNode } from 'react';

/**
 * Interface
 */
export enum BEHAVIOR_TYPE {
  HEIGHT = 'height',
  POSITION = 'position',
  PADDING = 'padding',
}

export interface KeyboardDismissViewProps {
  height?: number;
  children?: ReactNode[] | ReactNode;
  behaviorType?: BEHAVIOR_TYPE;
}
