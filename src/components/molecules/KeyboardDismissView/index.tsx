import React from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interface
 */
import { KeyboardDismissViewProps, BEHAVIOR_TYPE } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const KeyboardDismissView: React.FC<KeyboardDismissViewProps> = ({
  height,
  behaviorType = Platform.OS === 'ios'
    ? BEHAVIOR_TYPE.PADDING
    : BEHAVIOR_TYPE.HEIGHT,
  children,
}) => {
  return (
    <TouchableWithoutFeedback
      style={styles.container}
      onPress={Keyboard.dismiss}
    >
      <KeyboardAvoidingView
        behavior={behaviorType}
        keyboardVerticalOffset={height}
        style={styles.container}
      >
        {children}
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
};

export default KeyboardDismissView;

KeyboardDismissView.propTypes = PropTypes;
