import React from 'react';
import { Image } from 'react-native';
import { useNavigation, StackActions } from '@react-navigation/native';

/**
 * Icons
 */
import BACK from '../../../../assets/images/icons/back.png';

/**
 * Contexts
 */
import { useForgotPassword } from '../../../context/ForgotPasswordContext';

/**
 * Atoms
 */
import { ButtonWrapper } from '../../atoms/Button';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';
import { GoBackForgotPasswordButtonProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const GoBackForgotPasswordButton: React.FC<GoBackForgotPasswordButtonProps> = ({
  route,
}) => {
  const { forgotPasswordIndex, setForgotPasswordIndex } = useForgotPassword();

  /**
   * Navigation
   */
  const { dispatch } = useNavigation();
  const { replace } = StackActions;

  /**
   * Handlers
   */
  const handlePreviousPage = (value: number): void => {
    setForgotPasswordIndex(--value);
  };

  const handleNavigation = (): void => {
    dispatch(replace(route));
  };

  return (
    <ButtonWrapper
      buttonColor={COLORS.TRANSPARENT}
      onPress={() => {
        forgotPasswordIndex !== 0
          ? handlePreviousPage(forgotPasswordIndex)
          : handleNavigation();
      }}
    >
      <Image source={BACK} />
    </ButtonWrapper>
  );
};

export default GoBackForgotPasswordButton;

GoBackForgotPasswordButton.propTypes = PropTypes;
