export interface ToastProps {
  text: string;
  isError?: boolean;
  onPress: () => void;
  show?: boolean;
}
