import PropTypes from 'prop-types';

export default {
  show: PropTypes.bool,
  isError: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};
