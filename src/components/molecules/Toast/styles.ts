import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  toastContainer: {
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    bottom: 0,
    minHeight: 54,
    width: '100%',
    padding: 10,
  },
  toastDescription: {
    flex: 1,
    fontSize: 18,
    lineHeight: 24,
    color: COLORS.WHITE,
    marginRight: 10,
  },
  toastButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: 48,
  },
  toastButtonText: {
    fontSize: 18,
    lineHeight: 24,
    color: COLORS.WHITE,
  },
});

export default styles;
