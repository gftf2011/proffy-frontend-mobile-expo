import React from 'react';
import { View, Text } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import { ButtonWrapper } from '../../atoms/Button';

/**
 * Props
 */
import PropTypes from './propTypes';

/**
 * Interfaces
 */
import { COLORS as COLORS_BUTTON } from '../../atoms/Button/interfaces';
import { ToastProps } from './interfaces';

const Toast: React.FC<ToastProps> = ({
  show = false,
  isError = false,
  onPress,
  text,
}) => {
  return show ? (
    <View
      style={[
        styles.toastContainer,
        isError
          ? { backgroundColor: COLORS.IMPERIAL_RED }
          : { backgroundColor: COLORS.MEDIUM_STATE_BLUE },
      ]}
    >
      <Text style={styles.toastDescription}>{text}</Text>
      <ButtonWrapper buttonColor={COLORS_BUTTON.TRANSPARENT} onPress={onPress}>
        <Text style={styles.toastButtonText}>OK</Text>
      </ButtonWrapper>
    </View>
  ) : (
    <></>
  );
};

export default Toast;

Toast.propTypes = PropTypes;
