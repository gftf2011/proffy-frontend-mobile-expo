import React from 'react';
import { Image } from 'react-native';
import { useNavigation, StackActions } from '@react-navigation/native';

/**
 * Images
 */
import ARROW from '../../../../assets/images/icons/arrow.png';

/**
 * Contexts
 */
import { useOnboardPage } from '../../../context/OnboardPageContext';

/**
 * Atoms
 */
import { ButtonWrapper } from '../../atoms/Button';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';
import { NextOnboardButtonProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const NextOnboardButton: React.FC<NextOnboardButtonProps> = ({ route }) => {
  const {
    onboardIndex,
    onboardPagesLength,
    setOnboardIndex,
  } = useOnboardPage();

  /**
   * Navigation
   */
  const { dispatch } = useNavigation();
  const { replace } = StackActions;

  /**
   * Handlers
   */
  const handleNextPage = (value: number): void => {
    setOnboardIndex(++value);
  };

  const handleNavigation = (): void => {
    dispatch(replace(route));
  };

  return (
    <ButtonWrapper
      buttonColor={COLORS.TRANSPARENT}
      onPress={() => {
        onboardIndex < onboardPagesLength - 1
          ? handleNextPage(onboardIndex)
          : handleNavigation();
      }}
    >
      <Image source={ARROW} />
    </ButtonWrapper>
  );
};

export default NextOnboardButton;

NextOnboardButton.propTypes = PropTypes;
