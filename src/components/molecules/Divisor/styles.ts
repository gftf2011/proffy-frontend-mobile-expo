import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  divisor: {
    width: '100%',
    height: 1,
    backgroundColor: COLORS.MAGNOLIA,
  },
});

export default styles;
