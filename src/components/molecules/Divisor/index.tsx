import React from 'react';
import { View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interface
 */
import { DivisorProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const Divisor: React.FC<DivisorProps> = ({ verticalGap, horizontalGap }) => {
  return (
    <View
      style={[
        { marginVertical: verticalGap, marginHorizontal: horizontalGap },
        styles.divisor,
      ]}
    />
  );
};

export default Divisor;

Divisor.propTypes = PropTypes;
