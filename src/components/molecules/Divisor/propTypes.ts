import PropTypes from 'prop-types';

export default {
  verticalGap: PropTypes.number,
  horizontalGap: PropTypes.number,
};
