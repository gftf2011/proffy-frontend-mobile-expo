/**
 * Interface
 */
export interface DivisorProps {
  verticalGap?: number;
  horizontalGap?: number;
}
