import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  passwordContainer: {
    flexDirection: 'row',
  },
  passwordInputWrapper: {
    flex: 1,
    overflow: 'hidden',
    borderWidth: 2,
    fontFamily: 'Poppins_400Regular',
    color: COLORS.DARK_BLUE_GRAY,
    fontSize: 14,
    lineHeight: 24,
    alignItems: 'center',
  },
  passwordButton: {
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: 48,
  },
});

export const passwordInputSizeStyle = StyleSheet.create({
  small: {
    height: 24,
  },
  medium: {
    height: 54,
  },
});

export const passwordInputCornerStyle = StyleSheet.create({
  noCorner: {
    borderColor: COLORS.CULTURED,
  },
  withCorner: {
    borderColor: COLORS.MAGNOLIA,
  },
});

export const passwordInputBorderStyle = StyleSheet.create({
  noBorder: {
    borderRadius: 8,
  },
  withBorder: {
    borderRadius: 0,
  },
});

export default styles;
