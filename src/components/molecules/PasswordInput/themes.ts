import { ViewStyle } from 'react-native';

import {
  PASSWORD_INPUT_SIZE_TYPE,
  PASSWORD_INPUT_CORNER_TYPE,
  PASSWORD_INPUT_BORDER_TYPE,
} from './interfaces';

import {
  passwordInputBorderStyle,
  passwordInputCornerStyle,
  passwordInputSizeStyle,
} from './styles';

export const handlePasswordInputSizeType = (
  passwordInputSizeType?: PASSWORD_INPUT_SIZE_TYPE,
): ViewStyle => {
  const { SMALL, MEDIUM } = PASSWORD_INPUT_SIZE_TYPE;
  switch (passwordInputSizeType) {
    case SMALL:
      return passwordInputSizeStyle.small;
    case MEDIUM:
      return passwordInputSizeStyle.medium;
    default:
      return passwordInputSizeStyle.medium;
  }
};

export const handlePasswordInputCornerType = (
  passwordInputCornerType?: PASSWORD_INPUT_CORNER_TYPE,
): ViewStyle => {
  const { NO_CORNER, WITH_CORNER } = PASSWORD_INPUT_CORNER_TYPE;
  switch (passwordInputCornerType) {
    case NO_CORNER:
      return passwordInputCornerStyle.noCorner;
    case WITH_CORNER:
      return passwordInputCornerStyle.withCorner;
    default:
      return passwordInputCornerStyle.noCorner;
  }
};

export const handlePasswordInputBorderType = (
  passwordInputBorderType?: PASSWORD_INPUT_BORDER_TYPE,
): ViewStyle => {
  const { NO_BORDER, WITH_BORDER } = PASSWORD_INPUT_BORDER_TYPE;
  switch (passwordInputBorderType) {
    case NO_BORDER:
      return passwordInputBorderStyle.noBorder;
    case WITH_BORDER:
      return passwordInputBorderStyle.withBorder;
    default:
      return passwordInputBorderStyle.withBorder;
  }
};
