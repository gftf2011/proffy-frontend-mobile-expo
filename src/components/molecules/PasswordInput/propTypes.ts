import PropTypes from 'prop-types';
import {
  PASSWORD_INPUT_BORDER_TYPE,
  PASSWORD_INPUT_CORNER_TYPE,
  PASSWORD_INPUT_SIZE_TYPE,
} from './interfaces';

export default {
  passwordInputSize: PropTypes.oneOf([
    PASSWORD_INPUT_SIZE_TYPE.SMALL,
    PASSWORD_INPUT_SIZE_TYPE.MEDIUM,
  ]),
  passwordInputCorner: PropTypes.oneOf([
    PASSWORD_INPUT_CORNER_TYPE.NO_CORNER,
    PASSWORD_INPUT_CORNER_TYPE.WITH_CORNER,
  ]),
  passwordInputBorder: PropTypes.oneOf([
    PASSWORD_INPUT_BORDER_TYPE.NO_BORDER,
    PASSWORD_INPUT_BORDER_TYPE.WITH_BORDER,
  ]),
};
