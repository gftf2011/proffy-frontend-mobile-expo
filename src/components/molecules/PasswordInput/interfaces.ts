import { TextInputProps } from 'react-native';

/**
 * Enum
 */
export enum PASSWORD_INPUT_SIZE_TYPE {
  SMALL = 0,
  MEDIUM = 1,
}

export enum PASSWORD_INPUT_BORDER_TYPE {
  NO_BORDER = 0,
  WITH_BORDER = 1,
}

export enum PASSWORD_INPUT_CORNER_TYPE {
  NO_CORNER = 0,
  WITH_CORNER = 1,
}

export interface PasswordInputProps extends TextInputProps {
  passwordInputSize?: PASSWORD_INPUT_SIZE_TYPE;
  passwordInputCorner?: PASSWORD_INPUT_CORNER_TYPE;
  passwordInputBorder?: PASSWORD_INPUT_BORDER_TYPE;
}
