import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Input from '../../atoms/Input';
import Icon from '../../atoms/Icon';

/**
 * Themes
 */
import {
  handlePasswordInputBorderType,
  handlePasswordInputCornerType,
  handlePasswordInputSizeType,
} from './themes';

/**
 * Props
 */
import PropTypes from './propTypes';

/**
 * Interfaces
 */
import {
  INPUT_BORDER_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_SIZE_TYPE,
} from '../../atoms/Input/interfaces';
import { PasswordInputProps } from './interfaces';
import { IconType } from '../../atoms/Icon/interfaces';

const PasswordInput: React.FC<PasswordInputProps> = ({
  passwordInputSize,
  passwordInputCorner,
  passwordInputBorder,
  ...rest
}) => {
  const [passwordHidden, setPasswordHidden] = useState(true);

  return (
    <View style={styles.passwordContainer}>
      <View
        style={[
          styles.passwordInputWrapper,
          handlePasswordInputSizeType(passwordInputSize),
          handlePasswordInputCornerType(passwordInputCorner),
          handlePasswordInputBorderType(passwordInputBorder),
        ]}
      >
        <Input
          inputBorder={INPUT_BORDER_TYPE.WITH_BORDER}
          inputCorner={INPUT_CORNER_TYPE.NO_CORNER}
          inputSize={INPUT_SIZE_TYPE.MEDIUM}
          secureTextEntry={passwordHidden}
          keyboardType="decimal-pad"
          {...rest}
        />
      </View>
      <TouchableOpacity
        style={styles.passwordButton}
        onPress={() => {
          setPasswordHidden(!passwordHidden);
        }}
      >
        <Icon
          type={IconType.FEATHER}
          name={passwordHidden ? 'eye-off' : 'eye'}
          color={passwordHidden ? COLORS.MANATEE : COLORS.MEDIUM_STATE_BLUE}
          size={24}
        />
      </TouchableOpacity>
    </View>
  );
};

export default PasswordInput;

PasswordInput.propTypes = PropTypes;
