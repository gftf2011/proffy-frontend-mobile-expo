export interface TeacherSchedule {
  from: number;
  to: number;
  week_day: number;
}

export interface TeacherCardProps {
  id: number;
  picture?: string;
  subject: string;
  resumedName: string;
  bio?: string;
  schedule: TeacherSchedule[];
  cost: number;
  whatsapp: string;
  isFavorite?: boolean;
  onFavorite: (value: number) => Promise<void>;
  onUnfavorite: (value: number) => Promise<void>;
}
