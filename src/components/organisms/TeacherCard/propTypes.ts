import { ValidationMap } from 'react';
import PropTypes from 'prop-types';

/**
 * Interfaces
 */
import { TeacherSchedule } from './interfaces';

export default {
  id: PropTypes.number.isRequired,
  resumedName: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
  bio: PropTypes.string,
  teacherSchedule: PropTypes.arrayOf(
    PropTypes.shape<ValidationMap<TeacherSchedule>>({
      from: PropTypes.number.isRequired,
      to: PropTypes.number.isRequired,
      week_day: PropTypes.number.isRequired,
    }).isRequired,
  ),
  picture: PropTypes.string,
  cost: PropTypes.number.isRequired,
  whatsapp: PropTypes.string.isRequired,
  isFavorite: PropTypes.bool,
  onFavorite: PropTypes.func.isRequired,
  onUnfavorite: PropTypes.func.isRequired,
};
