import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    marginBottom: 17,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: COLORS.MAGNOLIA,
    width: '100%',
  },
  wrapper: {
    width: '100%',
    backgroundColor: COLORS.WHITE,
    padding: 20,
  },
  teacherBio: {
    marginTop: 30,
    fontFamily: 'Poppins_400Regular',
    fontSize: 14,
    lineHeight: 24,
    color: COLORS.DARK_BLUE_GRAY,
  },
  labelsContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dayLabel: {
    marginLeft: 15,
    fontFamily: 'Poppins_400Regular',
    fontSize: 10,
    lineHeight: 15,
    color: COLORS.MANATEE,
  },
  timeLabel: {
    marginRight: 50,
    fontFamily: 'Poppins_400Regular',
    fontSize: 10,
    lineHeight: 15,
    color: COLORS.MANATEE,
  },
  footer: {
    padding: 20,
    backgroundColor: COLORS.CULTURED,
  },
  priceContainer: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  priceLabel: {
    fontFamily: 'Poppins_400Regular',
    fontSize: 14,
    lineHeight: 24,
    color: COLORS.DARK_BLUE_GRAY,
  },
  price: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.MEDIUM_STATE_BLUE,
  },
  whatsappContainer: {
    marginTop: 24,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  favoriteTeacher: {
    marginRight: 7,
    height: 56,
    width: 56,
    alignItems: 'center',
    justifyContent: 'center',
  },
  whatsapp: {
    flex: 1,
  },
  innerButtonWrapper: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  whatsappText: {
    marginLeft: 16,
    fontFamily: 'Archivo_700Bold',
    fontSize: 14,
    lineHeight: 24,
    color: COLORS.WHITE,
  },
});

export default styles;
