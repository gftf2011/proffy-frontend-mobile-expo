import React, { useState, useEffect } from 'react';
import { View, Text, Image, Linking } from 'react-native';

/**
 * Services
 */
import api, { defaultHeaders } from '../../../services';

/**
 * Utils
 */
import { unmaskBrlPhone } from '../../../utils/StringUtils';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Button from '../../atoms/Button';

/**
 * Molecules
 */
import Divisor from '../../molecules/Divisor';
import ProfileSubject from '../../molecules/ProfileSubject';
import TeacherTimeInfo from '../../molecules/TeacherTimeInfo';

/**
 * Icons
 */
import WHATSAPP_ICON from '../../../../assets/images/icons/whatsapp.png';
import FAVORITE from '../../../../assets/images/icons/heart-outline.png';
import UNFAVORITE from '../../../../assets/images/icons/unfavorite.png';

/**
 * Constants
 */
import { RESUMED_TIME_OPTIONS } from '../../../constants/data';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';
import { TeacherCardProps } from './interfaces';

/**
 * Contexts
 */
import { useLoading } from '../../../context/LoadingContext';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const TeacherCard: React.FC<TeacherCardProps> = ({
  id,
  resumedName,
  subject,
  bio = '',
  whatsapp,
  schedule,
  picture = '',
  cost,
  onFavorite,
  onUnfavorite,
  isFavorite = false,
}) => {
  const { setIsLoading } = useLoading();
  const [isTeacherFavorite, setIsTeacherFavorite] = useState(isFavorite);

  /**
   * Effects
   */
  useEffect(() => {
    setIsTeacherFavorite(isFavorite);
  }, [isFavorite]);

  /**
   * Handlers
   */
  const handleWhatsappConverse = async (): Promise<void> => {
    try {
      setIsLoading(true);
      await api(defaultHeaders()).post('/connections', {
        user_id: id,
      });
      await Linking.openURL(`https://wa.me/+55${unmaskBrlPhone(whatsapp)}`);
    } catch (err) {
      console.warn(err);
    } finally {
      setIsLoading(false);
    }
  };

  const handleOnFavorite = async (teacherId: number): Promise<void> => {
    await onFavorite(teacherId);
  };

  const handleOnUnfavorite = async (teacherId: number): Promise<void> => {
    await onUnfavorite(teacherId);
  };

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <ProfileSubject
          resumedName={resumedName}
          subject={subject}
          picture={picture}
        />
        <Text style={styles.teacherBio}>{bio}</Text>
      </View>
      <Divisor />
      <View style={styles.wrapper}>
        <View style={styles.labelsContainer}>
          <Text style={styles.dayLabel}>Dia</Text>
          <Text style={styles.timeLabel}>Horário</Text>
        </View>
        {RESUMED_TIME_OPTIONS.map((timeOption, keyIndex) => {
          const index = schedule.findIndex(
            scheduleNow => scheduleNow.week_day === timeOption.value,
          );
          return (
            <TeacherTimeInfo
              key={keyIndex}
              disabled={
                !schedule.some(
                  scheduleNow => scheduleNow.week_day === timeOption.value,
                )
              }
              start={index !== -1 ? schedule[index].from : 0}
              end={index !== -1 ? schedule[index].to : 0}
              day={timeOption.label}
            />
          );
        })}
      </View>
      <Divisor />
      <View style={styles.footer}>
        <View style={styles.priceContainer}>
          <Text style={styles.priceLabel}>Preço / hora:</Text>
          <Text style={styles.price}>
            {new Intl.NumberFormat('pt-BR', {
              style: 'currency',
              currency: 'BRL',
            }).format(cost / 100)}
          </Text>
        </View>
        <View style={styles.whatsappContainer}>
          <View style={styles.favoriteTeacher}>
            <Button
              buttonColor={isTeacherFavorite ? COLORS.ERROR : COLORS.PRIMARY}
              onPress={() => {
                !isTeacherFavorite
                  ? handleOnFavorite(id)
                  : handleOnUnfavorite(id);
              }}
            >
              <Image source={isTeacherFavorite ? UNFAVORITE : FAVORITE} />
            </Button>
          </View>
          <View style={styles.whatsapp}>
            <Button
              buttonColor={COLORS.SECONDARY}
              onPress={handleWhatsappConverse}
            >
              <View style={styles.innerButtonWrapper}>
                <Image source={WHATSAPP_ICON} />
                <Text style={styles.whatsappText}>Entrar em contato</Text>
              </View>
            </Button>
          </View>
        </View>
      </View>
    </View>
  );
};

export default TeacherCard;

TeacherCard.propTypes = PropTypes;
