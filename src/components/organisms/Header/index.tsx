import React from 'react';
import { Text, Image } from 'react-native';

/**
 * Molecules
 */
import GoBackHeaderButton from '../../molecules/GoBackHeaderButton';

/**
 * Images
 */
import LOGO_IMAGE from '../../../../assets/images/logo.png';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interfaces
 */
import { HeaderInterface } from './interfaces';

const Header = (title: string): HeaderInterface => {
  const customOption: HeaderInterface = {
    title,
    headerTitle: () => <Text style={styles.titleText}>{title}</Text>,
    headerStyle: styles.headerStyle,
    headerLeft: () => <GoBackHeaderButton />,
    headerRight: () => (
      <Image
        style={{ marginRight: 33 }}
        source={LOGO_IMAGE}
        resizeMode="contain"
      />
    ),
    headerShown: true,
  };
  return customOption;
};

export default Header;
