import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  titleText: {
    fontSize: 14,
    lineHeight: 15,
    textAlign: 'center',
    fontFamily: 'Archivo_400Regular',
    color: '#d4c2ff',
  },
  headerStyle: {
    backgroundColor: '#774dd6',
  },
});

export default styles;
