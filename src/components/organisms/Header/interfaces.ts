export interface HeaderInterface {
  title: string;
  headerTitle: () => JSX.Element;
  headerStyle: {
    backgroundColor: string;
  };
  headerLeft: () => JSX.Element;
  headerRight: () => JSX.Element;
  headerShown: boolean;
}
