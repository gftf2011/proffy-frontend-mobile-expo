import PropTypes from 'prop-types';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export default {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  disabledButton: PropTypes.bool,
  buttonText: PropTypes.string.isRequired,
  buttonColor: PropTypes.oneOf([
    COLORS.PRIMARY,
    COLORS.SECONDARY,
    COLORS.ERROR,
    COLORS.TRANSPARENT,
  ]).isRequired,
  height: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired,
};
