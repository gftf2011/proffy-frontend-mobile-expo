import React from 'react';
import { View, Text } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Button from '../../atoms/Button';

/**
 * Molecules
 */
import GoBackForgotPasswordButton from '../../molecules/GoBackForgotPasswordButton';
import MarkerList from '../../molecules/MarkerList';

/**
 * Contexts
 */
import { useForgotPassword } from '../../../context/ForgotPasswordContext';

/**
 * Interfaces
 */
import { ForgotPasswordSectionProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const ForgotPasswordSection: React.FC<ForgotPasswordSectionProps> = ({
  children,
  title,
  subTitle,
  disabledButton = true,
  buttonColor,
  buttonText,
  height = 0,
  onPress,
}) => {
  const {
    forgotPasswordIndex,
    forgotPasswordPagesLength,
  } = useForgotPassword();
  return (
    <View style={styles.container}>
      <View style={styles.sectionHeader}>
        <GoBackForgotPasswordButton route="Login" />
        <MarkerList
          index={forgotPasswordIndex}
          size={forgotPasswordPagesLength}
        />
      </View>
      <View style={styles.mainSection}>
        {height === 0 && (
          <>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.subTitle}>{subTitle}</Text>
          </>
        )}
        <View style={styles.actionsContainer}>{children}</View>
        <Button
          isDisabled={disabledButton}
          buttonColor={buttonColor}
          onPress={onPress}
        >
          <Text style={styles.buttonText}>{buttonText}</Text>
        </Button>
      </View>
    </View>
  );
};

export default ForgotPasswordSection;

ForgotPasswordSection.propTypes = PropTypes;
