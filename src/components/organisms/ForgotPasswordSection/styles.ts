import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexGrow: 1,
    justifyContent: 'space-between',
  },
  sectionHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mainSection: {
    width: '100%',
    flexGrow: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-evenly',
  },
  title: {
    color: COLORS.RUSSIAN_VIOLET,
    fontFamily: 'Poppins_600SemiBold',
    fontSize: 28,
    lineHeight: 36,
    width: 250,
  },
  subTitle: {
    color: COLORS.DARK_BLUE_GRAY,
    fontSize: 14,
    lineHeight: 24,
    fontFamily: 'Poppins_400Regular',
    marginTop: 16,
    width: 260,
  },
  actionsContainer: {
    width: '100%',
  },
  buttonText: {
    fontFamily: 'Archivo_400Regular',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.WHITE,
  },
});

export default styles;
