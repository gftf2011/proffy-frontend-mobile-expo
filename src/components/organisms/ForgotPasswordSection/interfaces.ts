/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export interface ForgotPasswordSectionProps {
  title: string;
  subTitle: string;
  disabledButton?: boolean;
  buttonText: string;
  buttonColor: COLORS;
  height: number;
  onPress: () => Promise<void>;
}
