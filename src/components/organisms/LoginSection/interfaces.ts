export interface LoginSectionProps {
  height: number;
  onSubmit: () => Promise<void>;
}
