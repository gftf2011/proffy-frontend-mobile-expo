import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  titleSectionContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  titleLeft: {
    fontSize: 24,
    lineHeight: 34,
    fontFamily: 'Poppins_600SemiBold',
    color: COLORS.RUSSIAN_VIOLET,
  },
  titleRight: {
    color: COLORS.MEDIUM_STATE_BLUE,
    fontSize: 12,
    lineHeight: 24,
    fontFamily: 'Poppins_400Regular',
  },
  inputContainer: {
    overflow: 'hidden',
    width: '100%',
    backgroundColor: COLORS.CULTURED,
    borderColor: COLORS.MAGNOLIA,
    paddingVertical: 6,
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 12,
  },
  footerContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 24,
  },
  footerForgotPasswordText: {
    color: COLORS.MANATEE,
    fontSize: 12,
    lineHeight: 24,
    fontFamily: 'Poppins_400Regular',
  },
  loginButtonText: {
    fontFamily: 'Archivo_400Regular',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.WHITE,
  },
});

export default styles;
