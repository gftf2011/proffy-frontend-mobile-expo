import React from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { useNavigation } from '@react-navigation/native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Button, { ButtonWrapper } from '../../atoms/Button';
import Input from '../../atoms/Input';

/**
 * Molecules
 */
import PasswordInput from '../../molecules/PasswordInput';
import Divisor from '../../molecules/Divisor';
import RemenberMe from '../../molecules/RemenberMe';

/**
 * Context
 */
import { useRemenberMe } from '../../../context/RemenberMeContext';
import { useAuth } from '../../../context/AuthContext';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';
import {
  INPUT_BORDER_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_SIZE_TYPE,
} from '../../atoms/Input/interfaces';
import {
  PASSWORD_INPUT_BORDER_TYPE,
  PASSWORD_INPUT_CORNER_TYPE,
  PASSWORD_INPUT_SIZE_TYPE,
} from '../../molecules/PasswordInput/interfaces';

/**
 * Interfaces
 */
import { LoginSectionProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const LoginSection: React.FC<LoginSectionProps> = ({
  height = 0,
  onSubmit,
}) => {
  const { remenberMe } = useRemenberMe();
  const { setEmailToEdit, email, setEmail, password, setPassword } = useAuth();

  /**
   * Navigation
   */
  const { navigate } = useNavigation();

  /**
   * Handlers
   */
  const handleIsButtonDisabled = (): boolean => {
    return email.length === 0 || password.length === 0;
  };

  const handleClearTextFields = (): void => {
    setEmail(remenberMe ? email : '');
    setPassword('');
  };

  const onPress = async (): Promise<void> => {
    setEmailToEdit(email);
    onSubmit();
    handleClearTextFields();
  };

  return (
    <>
      {height === 0 && (
        <View style={styles.titleSectionContainer}>
          <Text style={styles.titleLeft}>Fazer Login</Text>
          <ButtonWrapper
            buttonColor={COLORS.TRANSPARENT}
            onPress={() => {
              navigate('SignIn');
            }}
          >
            <Text style={styles.titleRight}>Criar uma conta</Text>
          </ButtonWrapper>
        </View>
      )}
      <View style={styles.inputContainer}>
        <Input
          inputBorder={INPUT_BORDER_TYPE.WITH_BORDER}
          inputCorner={INPUT_CORNER_TYPE.NO_CORNER}
          inputSize={INPUT_SIZE_TYPE.MEDIUM}
          value={email}
          onChangeText={setEmail}
          placeholder="E-mail"
        />
        <Divisor verticalGap={6} />
        <PasswordInput
          passwordInputBorder={PASSWORD_INPUT_BORDER_TYPE.WITH_BORDER}
          passwordInputCorner={PASSWORD_INPUT_CORNER_TYPE.NO_CORNER}
          passwordInputSize={PASSWORD_INPUT_SIZE_TYPE.MEDIUM}
          value={password}
          onChangeText={setPassword}
          placeholder="Senha"
        />
      </View>
      {height === 0 && (
        <View style={styles.footerContainer}>
          <RemenberMe />
          <TouchableWithoutFeedback
            onPress={() => {
              navigate('ForgotPassword');
            }}
          >
            <Text style={styles.footerForgotPasswordText}>
              Esqueci minha senha
            </Text>
          </TouchableWithoutFeedback>
        </View>
      )}
      <Button
        isDisabled={handleIsButtonDisabled()}
        buttonColor={COLORS.SECONDARY}
        onPress={onPress}
      >
        <Text style={styles.loginButtonText}>Login</Text>
      </Button>
    </>
  );
};

export default LoginSection;

LoginSection.propTypes = PropTypes;
