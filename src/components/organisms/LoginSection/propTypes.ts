import PropTypes from 'prop-types';

export default {
  height: PropTypes.number.isRequired,
  onSubmit: PropTypes.func.isRequired,
};
