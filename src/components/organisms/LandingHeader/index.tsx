import React from 'react';
import { View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Contexts
 */
import { useUser } from '../../../context/UserContext';

/**
 * Molecules
 */
import ProfilePicture from '../../molecules/ProfilePicture';
import LogOffButton from '../../molecules/LogOffButton';

const LandingHeader: React.FC = () => {
  const { resumedName } = useUser();

  return (
    <View style={styles.container}>
      <ProfilePicture name={resumedName} />
      <LogOffButton route="Login" />
    </View>
  );
};

export default LandingHeader;
