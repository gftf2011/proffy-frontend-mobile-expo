import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  hero: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  heroTitle: {
    color: COLORS.RUSSIAN_VIOLET,
    fontFamily: 'Poppins_600SemiBold',
    fontSize: 32,
    lineHeight: 42,
    width: 236,
  },
  heroDescription: {
    color: COLORS.DARK_BLUE_GRAY,
    fontSize: 14,
    lineHeight: 24,
    fontFamily: 'Poppins_400Regular',
    marginTop: 16,
    width: 208,
  },
});

export default styles;
