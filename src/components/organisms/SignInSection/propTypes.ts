import PropTypes from 'prop-types';

export default {
  hide: PropTypes.bool,
};
