import React from 'react';
import { View, Text } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interfaces
 */
import { SignInSectionProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const SignInSection: React.FC<SignInSectionProps> = ({ hide = false }) => {
  return !hide ? (
    <View style={styles.hero}>
      <Text style={styles.heroTitle}>Crie sua conta gratuíta</Text>
      <Text style={styles.heroDescription}>
        Basta preencher esses dados e você estará conosco.
      </Text>
    </View>
  ) : (
    <></>
  );
};

export default SignInSection;

SignInSection.propTypes = PropTypes;
