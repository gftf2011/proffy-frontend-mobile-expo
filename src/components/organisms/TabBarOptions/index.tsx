import { BottomTabBarOptions } from '@react-navigation/bottom-tabs';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const TabBarOptions: BottomTabBarOptions = {
  style: {
    elevation: 0,
    shadowOpacity: 0,
    height: 64,
  },
  tabStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconStyle: {
    flex: 0,
    width: 20,
    height: 20,
  },
  labelStyle: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 13,
    marginLeft: 16,
  },
  inactiveBackgroundColor: COLORS.CULTURED,
  activeBackgroundColor: COLORS.LIGHT_MAGNOLIA,
  inactiveTintColor: COLORS.LAVENDER_GRAY,
  activeTintColor: COLORS.MEDIUM_STATE_BLUE,
};

export default TabBarOptions;
