export interface EditionFormProps {
  submit: () => Promise<void>;
}
