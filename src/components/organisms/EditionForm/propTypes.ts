import PropTypes from 'prop-types';

export default {
  submit: PropTypes.func.isRequired,
};
