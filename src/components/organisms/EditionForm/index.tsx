import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import { format } from 'date-fns';

/**
 * Storage
 */
import AsyncStorage from '@react-native-community/async-storage';

/**
 * Utils
 */
import {
  unmaskBrlCurrency,
  convertNumberToHourMinuteTimeFormat,
} from '../../../utils/StringUtils';

/**
 * Constants
 */
import { SUBJECT_OPTIONS, WORK_DAYS_ALLOWED } from '../../../constants/data';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Button, { ButtonWrapper } from '../../atoms/Button';
import Select from '../../atoms/Select';
import Input from '../../atoms/Input';
import MaskedInput from '../../atoms/MaskedInput';

/**
 * Molecules
 */
import Divisor from '../../molecules/Divisor';
import FormTimeSelectorsItem from '../../molecules/FormTimeSelectorsItem';

/**
 * Contexts
 */
import { useUser, OfficeHour } from '../../../context/UserContext';
import { useClasses } from '../../../context/ClassesContext';
import { useAuth } from '../../../context/AuthContext';
import { useLoading } from '../../../context/LoadingContext';

/**
 * Interfaces
 */
import { EditionFormProps } from './interfaces';
import { COLORS } from '../../atoms/Button/interfaces';
import {
  SELECT_BORDER_TYPE,
  SELECT_CORNER_TYPE,
  SELECT_SIZE_TYPE,
} from '../../atoms/Select/interfaces';
import {
  INPUT_BORDER_TYPE,
  INPUT_CORNER_TYPE,
  INPUT_SIZE_TYPE,
} from '../../atoms/Input/interfaces';
import {
  INPUT_BORDER_TYPE as MASK_INPUT_BORDER_TYPE,
  INPUT_CORNER_TYPE as MASK_INPUT_CORNER_TYPE,
  INPUT_SIZE_TYPE as MASK_INPUT_SIZE_TYPE,
  MASK_TYPE,
} from '../../atoms/MaskedInput/interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const EditionForm: React.FC<EditionFormProps> = ({ submit }) => {
  const {
    isTeacher,
    setOfficeHours,
    officeHours,
    setWhatsapp,
    whatsapp,
    setBio,
    bio,
    setName,
    name,
    setLastName,
    lastName,
  } = useUser();
  const { setSubject, subject, setCost, cost } = useClasses();
  const { setIsLoading } = useLoading();
  const { setEmailToEdit, emailToEdit } = useAuth();

  const [currency, setCurrency] = useState(cost.toString());
  const [workTimes, setWorkTimes] = useState(officeHours.length);

  /**
   * Effects
   */
  useEffect(() => {
    if (!isTeacher) {
      const today = new Date();
      const weekWorkTime: OfficeHour[] = [];

      weekWorkTime.push({
        id: 0,
        week_day: today.getDay(),
        from: format(today, 'HH:mm'),
        to: format(today, 'HH:mm'),
      });

      setOfficeHours(weekWorkTime);
      setWorkTimes(weekWorkTime.length);
      setSubject(SUBJECT_OPTIONS[0].label);
    } else {
      const fetch = async () => {
        try {
          setIsLoading(true);

          const data = await AsyncStorage.getItem('classSchedules');
          if (data) {
            const parsedData: OfficeHour[] = JSON.parse(data);

            setOfficeHours(
              parsedData.map(schedule => {
                const newSchedule: OfficeHour = {
                  id: schedule.id,
                  week_day: schedule.week_day,
                  from: convertNumberToHourMinuteTimeFormat(schedule.from),
                  to: convertNumberToHourMinuteTimeFormat(schedule.to),
                  class_id: schedule.class_id,
                };
                return newSchedule;
              }),
            );
          }
        } catch (err) {
          console.warn(err);
        } finally {
          setIsLoading(false);
        }
      };

      fetch();
    }
  }, []);

  useEffect(() => {
    setWorkTimes(officeHours.length);
  }, [officeHours]);

  useEffect(() => {
    setCost(parseFloat(unmaskBrlCurrency(currency)));
  }, [currency, setCost]);

  /**
   * Handlers
   */
  const handleAddNewOfficeHour = (): void => {
    const today = new Date();
    const weekWorkTime: OfficeHour[] = officeHours;

    const newTempId = officeHours[officeHours.length - 1].id;
    if (newTempId) {
      weekWorkTime.push({
        id: newTempId + 1,
        week_day: today.getDay(),
        from: format(today, 'HH:mm'),
        to: format(today, 'HH:mm'),
      });

      setOfficeHours(weekWorkTime);
      setWorkTimes(officeHours.length);
    }
  };

  const handleSubject = (
    itemValue: string | number,
    _itemIndex: number,
  ): void => {
    setSubject(itemValue.toString());
  };

  const handleIsButtonClickable = (): boolean => {
    if (isTeacher) {
      return (
        emailToEdit.length <= 0 ||
        name.length <= 0 ||
        lastName.length <= 0 ||
        bio.length <= 0 ||
        whatsapp.length <= 0
      );
    }
    return emailToEdit.length <= 0 || name.length <= 0 || lastName.length <= 0;
  };

  return (
    <View style={styles.container} onStartShouldSetResponder={() => true}>
      <View style={styles.mainContainer}>
        <Text style={styles.title}>Seus dados</Text>
        <Divisor verticalGap={20} />
        <Text style={styles.label}>Nome</Text>
        <Input
          inputBorder={INPUT_BORDER_TYPE.NO_BORDER}
          inputCorner={INPUT_CORNER_TYPE.WITH_CORNER}
          inputSize={INPUT_SIZE_TYPE.MEDIUM}
          placeholder={name}
          onChangeText={setName}
          value={name}
        />
        <Text style={styles.label}>Sobrenome</Text>
        <Input
          inputBorder={INPUT_BORDER_TYPE.NO_BORDER}
          inputCorner={INPUT_CORNER_TYPE.WITH_CORNER}
          inputSize={INPUT_SIZE_TYPE.MEDIUM}
          placeholder={lastName}
          onChangeText={setLastName}
          value={lastName}
        />
        <>
          <Text style={styles.label}>E-Mail</Text>
          <Input
            inputBorder={INPUT_BORDER_TYPE.NO_BORDER}
            inputCorner={INPUT_CORNER_TYPE.WITH_CORNER}
            inputSize={INPUT_SIZE_TYPE.MEDIUM}
            placeholder={emailToEdit}
            onChangeText={setEmailToEdit}
            value={emailToEdit}
          />
        </>
        {isTeacher ? (
          <>
            <Text style={styles.label}>WhatsApp</Text>
            <MaskedInput
              inputBorder={MASK_INPUT_BORDER_TYPE.NO_BORDER}
              inputCorner={MASK_INPUT_CORNER_TYPE.WITH_CORNER}
              inputSize={MASK_INPUT_SIZE_TYPE.MEDIUM}
              maskType={MASK_TYPE.BRL_CELLPHONE}
              placeholder="(99) 98765-1234"
              onChangeText={setWhatsapp}
              value={whatsapp}
            />
            <Text style={styles.label}>Biografia</Text>
            <Input
              inputBorder={INPUT_BORDER_TYPE.NO_BORDER}
              inputCorner={INPUT_CORNER_TYPE.WITH_CORNER}
              inputSize={INPUT_SIZE_TYPE.HIGH}
              placeholder="Sua história"
              multiline
              numberOfLines={12}
              onChangeText={setBio}
              value={bio}
            />
            <Text style={styles.title}>Sobre a aula</Text>
            <Divisor verticalGap={8} />
            <Text style={styles.label}>Matéria</Text>
            <Select
              selectBorder={SELECT_BORDER_TYPE.NO_BORDER}
              selectCorner={SELECT_CORNER_TYPE.WITH_CORNER}
              selectSize={SELECT_SIZE_TYPE.MEDIUM}
              options={SUBJECT_OPTIONS}
              onValueChange={handleSubject}
              selectedValue={subject}
            />
            <Text style={styles.label}>Custo hora por aula</Text>
            <MaskedInput
              inputBorder={MASK_INPUT_BORDER_TYPE.NO_BORDER}
              inputCorner={MASK_INPUT_CORNER_TYPE.WITH_CORNER}
              inputSize={MASK_INPUT_SIZE_TYPE.MEDIUM}
              maskType={MASK_TYPE.CURRENCY}
              placeholder="R$ 29,99"
              onChangeText={setCurrency}
              value={currency}
            />
            <View style={styles.availableTimeContainer}>
              <Text style={styles.title}>Horários disponíveis</Text>
              <ButtonWrapper
                disabled={workTimes < WORK_DAYS_ALLOWED}
                onPress={handleAddNewOfficeHour}
                buttonColor={COLORS.TRANSPARENT}
              >
                <Text style={styles.newAvailableTimeTextButton}>+ Novo</Text>
              </ButtonWrapper>
            </View>
            <Divisor verticalGap={8} />
            {officeHours.map((officeHour: OfficeHour, position: number) =>
              officeHour.id !== null && officeHour.id !== undefined ? (
                <FormTimeSelectorsItem
                  key={officeHour.id}
                  index={officeHour.id}
                  position={position}
                />
              ) : (
                <></>
              ),
            )}
          </>
        ) : (
          <></>
        )}
      </View>
      <View style={styles.footerContainer}>
        <Button
          isDisabled={handleIsButtonClickable()}
          onPress={submit}
          buttonColor={COLORS.SECONDARY}
        >
          <Text style={styles.signInButtonText}>Salvar alterações</Text>
        </Button>
      </View>
    </View>
  );
};

export default EditionForm;

EditionForm.propTypes = PropTypes;
