import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  label: {
    paddingVertical: 4,
    fontFamily: 'Poppins_400Regular',
    fontSize: 12,
    lineHeight: 22,
    color: COLORS.LAVENDER_BLUE,
  },
  innerHeaderSubject: {
    marginTop: 10,
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  innerHeaderTime: {
    marginBottom: 25,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  innerHeaderWeekDayWrapper: {
    width: '65%',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  innerHeaderTimeWrapper: {
    width: '32.5%',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  buttonSearchText: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.WHITE,
    letterSpacing: 1,
  },
});

export default styles;
