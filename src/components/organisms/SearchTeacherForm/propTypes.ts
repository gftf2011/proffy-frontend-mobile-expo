import PropTypes from 'prop-types';

export default {
  onPress: PropTypes.func.isRequired,
};
