import React, { useState } from 'react';
import { View, Text, Platform } from 'react-native';
import { format } from 'date-fns';

/**
 * Atoms
 */
import Button from '../../atoms/Button';
import Select from '../../atoms/Select';

/**
 * Molecules
 */
import TimePicker from '../../atoms/TimePicker';

/**
 * Styles
 */
import styles from './styles';

/**
 * Constants
 */
import { SUBJECT_OPTIONS, TIME_OPTIONS } from '../../../constants/data';

/**
 * Interfaces
 */
import { SearchTeacherFormProps } from './interfaces';
import { COLORS as BUTTON_COLORS } from '../../atoms/Button/interfaces';
import {
  SELECT_BORDER_TYPE,
  SELECT_CORNER_TYPE,
  SELECT_SIZE_TYPE,
} from '../../atoms/Select/interfaces';
import {
  TIMEPICKER_BORDER_TYPE,
  TIMEPICKER_CORNER_TYPE,
  TIMEPICKER_SIZE_TYPE,
} from '../../atoms/TimePicker/interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

/**
 * Contexts
 */
import { useTeacherList } from '../../../context/TeacherListContext';

const SearchTeacherForm: React.FC<SearchTeacherFormProps> = ({ onPress }) => {
  const [show, setShow] = useState(false);
  const [pickedTime, setPickedTime] = useState(new Date());
  const {
    setTime,
    setWeekDay,
    setSubject,
    weekDay,
    subject,
  } = useTeacherList();

  /**
   * Handlers
   */
  const handleTime = (date: Date): void => {
    setShow(Platform.OS === 'ios');
    setTime(format(date, 'HH:mm'));
    setPickedTime(date);
  };

  return (
    <View style={styles.container}>
      <View style={styles.innerHeaderSubject}>
        <Text style={styles.label}>Matéria</Text>
        <Select
          selectBorder={SELECT_BORDER_TYPE.NO_BORDER}
          selectCorner={SELECT_CORNER_TYPE.WITH_CORNER}
          selectSize={SELECT_SIZE_TYPE.MEDIUM}
          options={SUBJECT_OPTIONS}
          onValueChange={itemValue => {
            setSubject(itemValue as string);
          }}
          selectedValue={subject}
        />
      </View>
      <View style={styles.innerHeaderTime}>
        <View style={styles.innerHeaderWeekDayWrapper}>
          <Text style={styles.label}>Dia da semana</Text>
          <Select
            selectBorder={SELECT_BORDER_TYPE.NO_BORDER}
            selectCorner={SELECT_CORNER_TYPE.WITH_CORNER}
            selectSize={SELECT_SIZE_TYPE.MEDIUM}
            options={TIME_OPTIONS}
            onValueChange={itemValue => {
              setWeekDay(itemValue as number);
            }}
            selectedValue={weekDay}
          />
        </View>
        <View style={styles.innerHeaderTimeWrapper}>
          <Text style={styles.label}>Horário</Text>
          <TimePicker
            timePickerBorder={TIMEPICKER_BORDER_TYPE.NO_BORDER}
            timePickerCorner={TIMEPICKER_CORNER_TYPE.WITH_CORNER}
            timePickerSize={TIMEPICKER_SIZE_TYPE.MEDIUM}
            mode="time"
            is24Hour
            value={pickedTime}
            text={format(pickedTime, 'HH:mm')}
            show={show}
            onPress={() => {
              setShow(true);
            }}
            onChange={(_event: any, date: any) => {
              handleTime(date);
            }}
          />
        </View>
      </View>
      <Button onPress={onPress} buttonColor={BUTTON_COLORS.SECONDARY}>
        <Text style={styles.buttonSearchText}>Procurar</Text>
      </Button>
    </View>
  );
};

export default SearchTeacherForm;

SearchTeacherForm.propTypes = PropTypes;
