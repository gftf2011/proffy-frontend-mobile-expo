import React from 'react';
import { View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Molecules
 */
import MarkerList from '../../molecules/MarkerList';
import GoBackSignInButton from '../../molecules/GoBackSignInButton';

/**
 * Interfaces
 */
import { SignInHeaderProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const SignInHeader: React.FC<SignInHeaderProps> = ({ index = 0, size = 0 }) => {
  return (
    <View style={styles.header}>
      <GoBackSignInButton />
      <MarkerList index={index} size={size} />
    </View>
  );
};

export default SignInHeader;

SignInHeader.propTypes = PropTypes;
