import PropTypes from 'prop-types';

export default {
  size: PropTypes.number,
  index: PropTypes.number,
};
