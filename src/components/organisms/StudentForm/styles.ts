import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  mainContainer: {
    width: '100%',
    paddingHorizontal: 16,
    paddingBottom: 40,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.MAGNOLIA,
  },
  title: {
    marginTop: 40,
    color: COLORS.RUSSIAN_VIOLET,
    fontSize: 20,
    lineHeight: 30,
    fontFamily: 'Archivo_700Bold',
  },
  label: {
    marginTop: 16,
    marginBottom: 4,
    fontSize: 12,
    lineHeight: 22,
    fontFamily: 'Poppins_400Regular',
    color: COLORS.MANATEE,
  },
  availableTimeContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  newAvailableTimeTextButton: {
    fontSize: 14,
    lineHeight: 26,
    color: COLORS.MEDIUM_STATE_BLUE,
    fontFamily: 'Archivo_700Bold',
  },
  signInButtonText: {
    fontFamily: 'Archivo_400Regular',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.WHITE,
  },
  footerContainer: {
    paddingHorizontal: 16,
    paddingVertical: 24,
    width: '100%',
    backgroundColor: COLORS.CULTURED,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  footerInnerWrapper: {
    marginTop: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  footerInnerImportantMessage: {
    marginLeft: 16,
  },
  important: {
    fontFamily: 'Archivo_400Regular',
    fontSize: 12,
    lineHeight: 20,
    color: COLORS.MEDIUM_STATE_BLUE,
  },
  fillAllData: {
    fontFamily: 'Archivo_400Regular',
    fontSize: 12,
    lineHeight: 20,
    color: COLORS.MANATEE2,
  },
});

export default styles;
