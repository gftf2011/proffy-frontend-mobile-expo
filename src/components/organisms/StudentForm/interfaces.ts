export interface StudentFormProps {
  submit: () => Promise<void>;
}
