export interface OnboardSectionProps {
  pageNumber: string;
  text: string;
}
