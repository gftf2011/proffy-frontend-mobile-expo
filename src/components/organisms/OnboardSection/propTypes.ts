import PropTypes from 'prop-types';

export default {
  pageNumber: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};
