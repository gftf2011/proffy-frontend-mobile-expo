import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  footer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  onboardPage: {
    fontFamily: 'Archivo_400Regular',
    color: COLORS.DARK_BLUE_GRAY,
    opacity: 0.16,
    fontSize: 40,
    lineHeight: 44,
    marginBottom: 24,
  },
  onboardDescription: {
    fontFamily: 'Poppins_400Regular',
    color: COLORS.DARK_BLUE_GRAY,
    fontSize: 24,
    lineHeight: 32,
    width: 208,
    marginBottom: 96,
  },
});

export default styles;
