import React from 'react';
import { View, Text } from 'react-native';

/**
 * Molecules
 */
import MarkerList from '../../molecules/MarkerList';
import NextOnboardButton from '../../molecules/NextOnboardButton';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interfaces
 */
import { OnboardSectionProps } from './interfaces';

/**
 * Contexts
 */
import { useOnboardPage } from '../../../context/OnboardPageContext';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const OnboardSection: React.FC<OnboardSectionProps> = ({
  pageNumber,
  text,
}) => {
  const { onboardIndex, onboardPagesLength } = useOnboardPage();

  return (
    <>
      <Text style={styles.onboardPage}>{pageNumber}</Text>
      <Text style={styles.onboardDescription}>{text}</Text>
      <View style={styles.footer}>
        <MarkerList index={onboardIndex} size={onboardPagesLength} />
        <NextOnboardButton route="Login" />
      </View>
    </>
  );
};

export default OnboardSection;

OnboardSection.propTypes = PropTypes;
