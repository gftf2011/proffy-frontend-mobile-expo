import React from 'react';
import { View, Text, Image, ScrollView } from 'react-native';

/**
 * Icons
 */
import HAPPY_EMOJI_WITH_HEART_EYES from '../../../../assets/images/icons/happy-emoji-with-heart-eyes.png';

/**
 * Styles
 */
import styles from './styles';

/**
 * Molecules
 */
import Toast from '../../molecules/Toast';
import Loader from '../../molecules/Loader';

/**
 * Organisms
 */
import TeacherCard from '../../organisms/TeacherCard';

/**
 * Interfaces
 */
import { FavoriteTeachersTemplateProps } from './interfaces';

/**
 * Contexts
 */
import { useFavoriteTeacherList } from '../../../context/FavoriteTeacherListContext';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const FavoriteTeachersTemplate: React.FC<FavoriteTeachersTemplateProps> = ({
  onPress,
  onFavorite,
  onUnfavorite,
  showToast = false,
  isError = false,
  message = '',
}) => {
  const { teachers } = useFavoriteTeacherList();

  return (
    <>
      <Loader />
      <View style={styles.container}>
        <View style={styles.headerSection}>
          <Text style={styles.title}>Meus proffs Favoritos</Text>
          <View style={styles.counter}>
            <Image source={HAPPY_EMOJI_WITH_HEART_EYES} />
            <Text style={styles.counterText}>
              {`${teachers.length} proffs`}
            </Text>
          </View>
        </View>
        <View style={styles.scrollContainer}>
          <ScrollView style={styles.scroll}>
            {teachers.length > 0 &&
              teachers.map((teacher, index) => (
                <TeacherCard
                  id={teacher.id}
                  resumedName={teacher.resumedName}
                  whatsapp={teacher.whatsapp}
                  cost={teacher.cost}
                  subject={teacher.subject}
                  bio={teacher.bio}
                  picture={teacher.picture}
                  schedule={teacher.schedule}
                  isFavorite={teacher.isFavorite}
                  key={index}
                  onUnfavorite={onUnfavorite}
                  onFavorite={onFavorite}
                />
              ))}
          </ScrollView>
        </View>
      </View>
      <Toast
        onPress={onPress}
        isError={isError}
        show={showToast}
        text={message}
      />
    </>
  );
};

export default FavoriteTeachersTemplate;

FavoriteTeachersTemplate.propTypes = PropTypes;
