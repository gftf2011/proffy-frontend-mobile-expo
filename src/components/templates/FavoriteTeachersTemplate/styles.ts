import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  headerSection: {
    flexDirection: 'row',
    paddingTop: 32,
    paddingHorizontal: 32,
    paddingBottom: 80,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
  },
  title: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 24,
    lineHeight: 29,
    color: COLORS.WHITE,
    width: 150,
  },
  counter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  counterText: {
    marginLeft: 8,
    fontFamily: 'Poppins_400Regular',
    fontSize: 12,
    lineHeight: 18,
    color: COLORS.LAVENDER_BLUE,
  },
  scrollContainer: {
    flex: 1,
    paddingHorizontal: 32,
  },
  scroll: {
    marginTop: -55,
    borderRadius: 8,
  },
});

export default styles;
