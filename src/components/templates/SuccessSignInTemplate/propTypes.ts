import PropTypes from 'prop-types';

export default {
  routeToGo: PropTypes.string.isRequired,
};
