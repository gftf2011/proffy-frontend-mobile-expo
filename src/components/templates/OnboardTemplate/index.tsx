import React from 'react';
import { View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Organisms
 */
import OnboardSection from '../../organisms/OnboardSection';

/**
 * Interfaces
 */
import { OnboardTemplateProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const OnboardTemplate: React.FC<OnboardTemplateProps> = ({
  pageNumber,
  text,
  image,
  color,
}) => {
  return (
    <View style={styles.container}>
      <View style={[styles.header, { backgroundColor: color }]}>{image}</View>
      <View style={styles.onboardContainer}>
        <OnboardSection pageNumber={pageNumber} text={text} />
      </View>
    </View>
  );
};

export default OnboardTemplate;

OnboardTemplate.propTypes = PropTypes;
