import PropTypes from 'prop-types';

export default {
  image: PropTypes.node.isRequired,
  pageNumber: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
};
