import { ReactNode } from 'react';

export interface OnboardTemplateProps {
  image: ReactNode;
  pageNumber: string;
  text: string;
  color: string;
}
