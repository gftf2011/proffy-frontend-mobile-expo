import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  header: {
    height: '40%',
    paddingTop: 50,
    paddingHorizontal: 50,
    paddingBottom: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  onboardContainer: {
    height: '60%',
    paddingHorizontal: 32,
    alignItems: 'flex-start',
    justifyContent: 'space-evenly',
  },
});

export default styles;
