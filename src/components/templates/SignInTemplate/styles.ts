import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  wrapper: {
    flex: 1,
    paddingVertical: 48,
    paddingHorizontal: 32,
    justifyContent: 'space-between',
  },
  title: {
    color: COLORS.RUSSIAN_VIOLET,
    fontSize: 24,
    lineHeight: 30,
    marginBottom: 24,
    fontFamily: 'Poppins_600SemiBold',
  },
  actionsContainer: {
    width: '100%',
  },
  buttonText: {
    fontFamily: 'Archivo_400Regular',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.WHITE,
  },
});

export default styles;
