import { ReactNode } from 'react';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export interface SignInTemplateProps {
  text: string;
  buttonText: string;
  buttonColor: COLORS;
  children: ReactNode;
  disabledButton?: boolean;
  showToast?: boolean;
  isError?: boolean;
  message?: string;
  onPress: () => void;
  signIn: (index: number) => Promise<void>;
}
