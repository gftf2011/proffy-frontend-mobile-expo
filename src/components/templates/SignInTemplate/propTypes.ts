import PropTypes from 'prop-types';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export default {
  showToast: PropTypes.bool,
  isError: PropTypes.bool,
  disabledButton: PropTypes.bool,
  text: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  buttonColor: PropTypes.oneOf([
    COLORS.PRIMARY,
    COLORS.SECONDARY,
    COLORS.ERROR,
    COLORS.TRANSPARENT,
  ]).isRequired,
  children: PropTypes.node.isRequired,
  onPress: PropTypes.func.isRequired,
  signIn: PropTypes.func.isRequired,
};
