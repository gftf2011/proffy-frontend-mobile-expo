import React from 'react';
import { View, Text } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Button from '../../atoms/Button';

/**
 * Molecules
 */
import Toast from '../../molecules/Toast';
import Loader from '../../molecules/Loader';
import KeyboardDismissView from '../../molecules/KeyboardDismissView';

/**
 * Organisms
 */
import SignInHeader from '../../organisms/SignInHeader';
import SignInSection from '../../organisms/SignInSection';

/**
 * Hooks
 */
import useKeyboard from '../../../hooks/useKeyboard';

/**
 * Contexts
 */
import { useSignInPage } from '../../../context/SignInPageContext';

/**
 * Interfaces
 */
import { SignInTemplateProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const SignInTemplate: React.FC<SignInTemplateProps> = ({
  showToast = false,
  isError = false,
  disabledButton = true,
  message = '',
  text,
  buttonText,
  buttonColor,
  children,
  onPress,
  signIn,
}) => {
  const [height] = useKeyboard();

  const { signInIndex, signInPagesLength } = useSignInPage();

  return (
    <>
      <Loader />
      <View style={styles.container}>
        <KeyboardDismissView height={height}>
          <View style={styles.wrapper}>
            <SignInHeader index={signInIndex} size={signInPagesLength} />
            <SignInSection hide={height > 0} />
            <View style={styles.actionsContainer}>
              <Text style={styles.title}>{text}</Text>
              {children}
            </View>
            <Button
              isDisabled={disabledButton}
              buttonColor={buttonColor}
              onPress={() => {
                signIn(signInIndex);
              }}
            >
              <Text style={styles.buttonText}>{buttonText}</Text>
            </Button>
          </View>
        </KeyboardDismissView>
      </View>
      <Toast
        onPress={onPress}
        isError={isError}
        show={showToast}
        text={message}
      />
    </>
  );
};

export default SignInTemplate;

SignInTemplate.propTypes = PropTypes;
