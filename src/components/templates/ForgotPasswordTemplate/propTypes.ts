import PropTypes from 'prop-types';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export default {
  showToast: PropTypes.bool,
  isError: PropTypes.bool,
  message: PropTypes.string,
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  disabledButton: PropTypes.bool,
  buttonText: PropTypes.string.isRequired,
  buttonColor: PropTypes.oneOf([
    COLORS.PRIMARY,
    COLORS.SECONDARY,
    COLORS.ERROR,
    COLORS.TRANSPARENT,
  ]).isRequired,
  children: PropTypes.node.isRequired,
  onPress: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};
