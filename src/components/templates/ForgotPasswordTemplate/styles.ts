import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  header: {
    height: '40%',
    paddingTop: 50,
    paddingHorizontal: 50,
    paddingBottom: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
  },
  hero: {
    flex: 1,
  },
  forgotPasswordContainer: {
    height: '60%',
    paddingHorizontal: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
