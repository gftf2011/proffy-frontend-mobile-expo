/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';

export interface ForgotPasswordTemplateProps {
  showToast?: boolean;
  isError?: boolean;
  message?: string;
  title: string;
  subTitle: string;
  disabledButton?: boolean;
  buttonText: string;
  buttonColor: COLORS;
  onPress: () => void;
  onSubmit: () => Promise<void>;
}
