import React from 'react';
import { Image, View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Images
 */
import PROFFY_MOBILE_HERO from '../../../../assets/images/proffy-mobile-hero.png';

/**
 * Molecules
 */
import Toast from '../../molecules/Toast';
import Loader from '../../molecules/Loader';
import KeyboardDismissView from '../../molecules/KeyboardDismissView';

/**
 * Organisms
 */
import ForgotPasswordSection from '../../organisms/ForgotPasswordSection';

/**
 * Hooks
 */
import useKeyboard from '../../../hooks/useKeyboard';

/**
 * Interfaces
 */
import { ForgotPasswordTemplateProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const ForgotPasswordTemplate: React.FC<ForgotPasswordTemplateProps> = ({
  showToast = false,
  isError = false,
  message = '',
  title,
  subTitle,
  disabledButton = true,
  buttonText,
  buttonColor,
  children,
  onPress,
  onSubmit,
}) => {
  const [height] = useKeyboard();

  return (
    <>
      <Loader />
      <View style={styles.container}>
        <KeyboardDismissView height={height}>
          <View style={styles.header}>
            <Image
              source={PROFFY_MOBILE_HERO}
              resizeMode={height === 0 ? 'contain' : 'cover'}
              style={styles.hero}
            />
          </View>
          <View style={styles.forgotPasswordContainer}>
            <ForgotPasswordSection
              disabledButton={disabledButton}
              buttonColor={buttonColor}
              buttonText={buttonText}
              title={title}
              subTitle={subTitle}
              height={height}
              onPress={onSubmit}
            >
              {children}
            </ForgotPasswordSection>
          </View>
        </KeyboardDismissView>
      </View>
      <Toast
        onPress={onPress}
        isError={isError}
        show={showToast}
        text={message}
      />
    </>
  );
};

export default ForgotPasswordTemplate;

ForgotPasswordTemplate.propTypes = PropTypes;
