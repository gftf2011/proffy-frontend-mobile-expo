export interface GiveClassesTemplateProps {
  showToast?: boolean;
  isError?: boolean;
  message?: string;
  onPress: () => void;
  submit: () => Promise<void>;
}
