import React from 'react';
import {
  Keyboard,
  TouchableWithoutFeedback,
  ScrollView,
  View,
  Text,
} from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Molecules
 */
import Toast from '../../molecules/Toast';
import Loader from '../../molecules/Loader';

/**
 * Hooks
 */
import useKeyboard from '../../../hooks/useKeyboard';

/**
 * Organisms
 */
import StudentForm from '../../organisms/StudentForm';

/**
 * Interfaces
 */
import { GiveClassesTemplateProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const GiveClassesTemplate: React.FC<GiveClassesTemplateProps> = ({
  showToast = false,
  isError = false,
  message = '',
  onPress,
  submit,
}) => {
  const [height] = useKeyboard();

  return (
    <>
      <Loader />
      <TouchableWithoutFeedback
        style={styles.container}
        onPress={Keyboard.dismiss}
      >
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.title}>
              Que incrível que você queira dar aulas.
            </Text>
            {height === 0 && (
              <Text style={styles.subTitle}>
                O primeiro passo, é preencher esse formulário de inscrição.
              </Text>
            )}
          </View>
          <View style={styles.section}>
            <ScrollView style={styles.formContainer}>
              <StudentForm submit={submit} />
            </ScrollView>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <Toast
        onPress={onPress}
        isError={isError}
        show={showToast}
        text={message}
      />
    </>
  );
};

export default GiveClassesTemplate;

GiveClassesTemplate.propTypes = PropTypes;
