import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  header: {
    height: '40%',
    padding: 32,
    alignItems: 'flex-start',
    justifyContent: 'space-evenly',
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
  },
  title: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 24,
    lineHeight: 29,
    width: 250,
    color: COLORS.WHITE,
  },
  subTitle: {
    fontFamily: 'Poppins_400Regular',
    fontSize: 14,
    lineHeight: 24,
    width: 250,
    color: COLORS.LAVENDER_BLUE,
  },
  section: {
    height: '60%',
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  formContainer: {
    marginTop: -24,
    paddingVertical: 12,
    height: '100%',
    width: '100%',
    backgroundColor: COLORS.WHITE,
    borderRadius: 8,
  },
});

export default styles;
