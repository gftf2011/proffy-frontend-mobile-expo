import React from 'react';
import {
  Keyboard,
  TouchableWithoutFeedback,
  ScrollView,
  View,
} from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Molecules
 */
import Toast from '../../molecules/Toast';
import Loader from '../../molecules/Loader';

/**
 * Hooks
 */
import useKeyboard from '../../../hooks/useKeyboard';

/**
 * Molecules
 */
import ProfilePictureEdit from '../../molecules/ProfilePictureEdit';

/**
 * Organisms
 */
import EditionForm from '../../organisms/EditionForm';

/**
 * Interfaces
 */
import { ProfileTemplateProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const ProfileTemplate: React.FC<ProfileTemplateProps> = ({
  showToast = false,
  isError = false,
  message = '',
  onPress,
  submit,
}) => {
  const [height] = useKeyboard();

  return (
    <>
      <Loader />
      <TouchableWithoutFeedback
        style={styles.container}
        onPress={Keyboard.dismiss}
      >
        <View style={styles.container}>
          <View style={styles.header}>
            {height === 0 ? <ProfilePictureEdit /> : <></>}
          </View>
          <View style={styles.section}>
            <ScrollView style={styles.formContainer}>
              <EditionForm submit={submit} />
            </ScrollView>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <Toast
        onPress={onPress}
        isError={isError}
        show={showToast}
        text={message}
      />
    </>
  );
};

export default ProfileTemplate;

ProfileTemplate.propTypes = PropTypes;
