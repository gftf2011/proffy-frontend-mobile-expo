export interface ProfileTemplateProps {
  showToast?: boolean;
  isError?: boolean;
  message?: string;
  onPress: () => void;
  submit: () => Promise<void>;
}
