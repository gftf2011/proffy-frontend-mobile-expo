import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  header: {
    height: '50%',
    padding: 32,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
  },
  section: {
    height: '50%',
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  formContainer: {
    marginTop: -24,
    paddingVertical: 12,
    height: '100%',
    width: '100%',
    backgroundColor: COLORS.WHITE,
    borderRadius: 8,
  },
});

export default styles;
