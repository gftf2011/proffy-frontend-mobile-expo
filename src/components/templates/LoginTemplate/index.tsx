import React from 'react';
import { Image, View } from 'react-native';

/**
 * Styles
 */
import styles from './styles';

/**
 * Images
 */
import PROFFY_MOBILE_HERO from '../../../../assets/images/proffy-mobile-hero.png';

/**
 * Molecules
 */
import Toast from '../../molecules/Toast';
import Loader from '../../molecules/Loader';
import KeyboardDismissView from '../../molecules/KeyboardDismissView';

/**
 * Organisms
 */
import LoginSection from '../../organisms/LoginSection';

/**
 * Hooks
 */
import useKeyboard from '../../../hooks/useKeyboard';

/**
 * Interfaces
 */
import { LoginTemplateProps } from './interfaces';

/**
 * Props
 */
import PropTypes from './propTypes';

const LoginTemplate: React.FC<LoginTemplateProps> = ({
  showToast = false,
  isError = false,
  message = '',
  onPress,
  onSubmit,
}) => {
  const [height] = useKeyboard();

  return (
    <>
      <Loader />
      <View style={styles.container}>
        <KeyboardDismissView height={height}>
          <View style={styles.header}>
            <Image
              source={PROFFY_MOBILE_HERO}
              resizeMode={height === 0 ? 'contain' : 'cover'}
              style={styles.hero}
            />
          </View>
          <View style={styles.loginContainer}>
            <LoginSection height={height} onSubmit={onSubmit} />
          </View>
        </KeyboardDismissView>
      </View>
      <Toast
        onPress={onPress}
        isError={isError}
        show={showToast}
        text={message}
      />
    </>
  );
};

export default LoginTemplate;

LoginTemplate.propTypes = PropTypes;
