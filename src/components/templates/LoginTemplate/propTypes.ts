import PropTypes from 'prop-types';

export default {
  showToast: PropTypes.bool,
  isError: PropTypes.bool,
  message: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};
