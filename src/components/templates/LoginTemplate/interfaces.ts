export interface LoginTemplateProps {
  showToast?: boolean;
  isError?: boolean;
  message?: string;
  onPress: () => void;
  onSubmit: () => Promise<void>;
}
