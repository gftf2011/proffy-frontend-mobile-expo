import PropTypes from 'prop-types';

export default {
  showToast: PropTypes.bool,
  isError: PropTypes.bool,
  message: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  onFavorite: PropTypes.func.isRequired,
  onUnfavorite: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
};
