import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    paddingTop: 32,
    paddingHorizontal: 32,
    paddingBottom: 80,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
  },
  header: {
    marginBottom: 20,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontFamily: 'Archivo_700Bold',
    fontSize: 24,
    lineHeight: 29,
    color: COLORS.WHITE,
    width: 140,
  },
  counter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  counterText: {
    marginLeft: 8,
    fontFamily: 'Poppins_400Regular',
    fontSize: 12,
    lineHeight: 18,
    color: COLORS.LAVENDER_BLUE,
  },
  buttonInnerWrapper: {
    paddingVertical: 10,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.MEDIUM_PURPLE,
  },
  buttonInnerWrapperContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonText: {
    marginLeft: 20,
    fontFamily: 'Archivo_400Regular',
    color: COLORS.LAVENDER_BLUE,
    fontSize: 16,
    lineHeight: 19,
  },
  scrollContainer: {
    flex: 1,
    paddingHorizontal: 32,
  },
  scroll: {
    marginTop: -55,
    borderRadius: 8,
  },
});

export default styles;
