import React, { useState, useEffect } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

/**
 * Atoms
 */
import Button from '../../atoms/Button';
import Icon from '../../atoms/Icon';

/**
 * Molecules
 */
import Toast from '../../molecules/Toast';
import Loader from '../../molecules/Loader';

/**
 * Organisms
 */
import TeacherCard from '../../organisms/TeacherCard';
import SearchTeacherForm from '../../organisms/SearchTeacherForm';

/**
 * Icons
 */
import HAPPY_EMOJI_WITH_GLASS from '../../../../assets/images/icons/happy-emoji-with-glass.png';

/**
 * Styles
 */
import styles from './styles';

/**
 * Interfaces
 */
import { TeacherListTemplateProps } from './interfaces';
import { COLORS as BUTTON_COLORS } from '../../atoms/Button/interfaces';
import { IconType } from '../../atoms/Icon/interfaces';

/**
 * Contexts
 */
import { useTeacherList } from '../../../context/TeacherListContext';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const TeacherListTemplate: React.FC<TeacherListTemplateProps> = ({
  onPress,
  onSearch,
  onFavorite,
  onUnfavorite,
  showToast = false,
  isError = false,
  message = '',
}) => {
  const [openFilter, setOpenFilter] = useState(false);
  const [educators, setEducators] = useState(0);

  const { teachers } = useTeacherList();

  /**
   * Effects
   */
  useEffect(() => {
    setEducators(teachers.length);
  }, [onSearch, teachers.length]);

  /**
   * Handlers
   */
  const handleClickFilterButton = (isOpened: boolean): void => {
    setOpenFilter(!isOpened);
  };

  const handleSearch = async (): Promise<void> => {
    setOpenFilter(false);
    await onSearch();
    setEducators(teachers.length);
  };

  return (
    <>
      <Loader />
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.header}>
            <Text style={styles.title}>Proffs Disponíveis</Text>
            <View style={styles.counter}>
              <Image source={HAPPY_EMOJI_WITH_GLASS} />
              <Text style={styles.counterText}>{`${educators} proffs`}</Text>
            </View>
          </View>
          <Button
            onPress={() => {
              handleClickFilterButton(openFilter);
            }}
            buttonColor={BUTTON_COLORS.TRANSPARENT}
          >
            <View style={styles.buttonInnerWrapper}>
              <View style={styles.buttonInnerWrapperContainer}>
                <Icon
                  type={IconType.FEATHER}
                  name="filter"
                  size={24}
                  color={COLORS.MALACHITE}
                />
                <Text style={styles.buttonText}>
                  Filtrar por campos avançados
                </Text>
              </View>
              <Icon
                type={IconType.FEATHER}
                name={openFilter ? 'chevron-up' : 'chevron-down'}
                size={24}
                color={COLORS.LIGHT_MEDIUM_PURPLE}
              />
            </View>
          </Button>
          {openFilter && <SearchTeacherForm onPress={handleSearch} />}
        </View>
        <View style={styles.scrollContainer}>
          <ScrollView style={styles.scroll}>
            {teachers.length > 0 &&
              teachers.map((teacher, index) => (
                <TeacherCard
                  id={teacher.id}
                  resumedName={teacher.resumedName}
                  whatsapp={teacher.whatsapp}
                  cost={teacher.cost}
                  subject={teacher.subject}
                  bio={teacher.bio}
                  picture={teacher.picture}
                  schedule={teacher.schedule}
                  isFavorite={teacher.isFavorite}
                  key={index}
                  onUnfavorite={onUnfavorite}
                  onFavorite={onFavorite}
                />
              ))}
          </ScrollView>
        </View>
      </View>
      <Toast
        onPress={onPress}
        isError={isError}
        show={showToast}
        text={message}
      />
    </>
  );
};

export default TeacherListTemplate;

TeacherListTemplate.propTypes = PropTypes;
