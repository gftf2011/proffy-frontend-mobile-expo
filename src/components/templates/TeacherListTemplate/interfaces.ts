export interface TeacherListTemplateProps {
  showToast?: boolean;
  isError?: boolean;
  message?: string;
  onPress: () => void;
  onFavorite: (value: number) => Promise<void>;
  onUnfavorite: (value: number) => Promise<void>;
  onSearch: () => Promise<void>;
}
