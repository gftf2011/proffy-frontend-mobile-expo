import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
    paddingHorizontal: 40,
    paddingBottom: 40,
  },
  backgroundImage: {
    flex: 1,
    marginBottom: 54,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginTop: 36,
    marginBottom: 16,
    width: 210,
    fontSize: 32,
    lineHeight: 37,
    fontFamily: 'Archivo_700Bold',
    textAlign: 'center',
    color: COLORS.WHITE,
  },
  description: {
    width: 190,
    fontSize: 14,
    lineHeight: 24,
    fontFamily: 'Poppins_400Regular',
    textAlign: 'center',
    color: COLORS.LAVENDER_BLUE,
  },
  doLoginButtonText: {
    fontFamily: 'Archivo_400Regular',
    fontSize: 16,
    lineHeight: 26,
    color: COLORS.WHITE,
  },
});

export default styles;
