import React from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';
import { useNavigation, StackActions } from '@react-navigation/native';

/**
 * Images
 */
import SUCCESS_SIGN_IN_BACKGROUND from '../../../../assets/images/success-sign-in-background.png';
import DONE from '../../../../assets/images/done.png';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Button from '../../atoms/Button';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';
import { SuccessForgotPasswordTemplateProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const SuccessForgotPasswordTemplate: React.FC<SuccessForgotPasswordTemplateProps> = ({
  routeToGo,
}) => {
  /**
   * Navigation
   */
  const { replace } = StackActions;
  const { dispatch } = useNavigation();

  /**
   * Handlers
   */
  const handleNavigation = (): void => {
    dispatch(replace(routeToGo));
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        style={styles.backgroundImage}
        resizeMode="contain"
        source={SUCCESS_SIGN_IN_BACKGROUND}
      >
        <Image source={DONE} />
        <Text style={styles.title}>Redefinição concluída!</Text>
        <Text style={styles.description}>
          Boa, agora que você redefiniu sua senha com sucesso é só voltar para
          página de Login e aproveitar os estudos!
        </Text>
      </ImageBackground>
      <Button buttonColor={COLORS.SECONDARY} onPress={handleNavigation}>
        <Text style={styles.doLoginButtonText}>Fazer Login</Text>
      </Button>
    </View>
  );
};

export default SuccessForgotPasswordTemplate;

SuccessForgotPasswordTemplate.propTypes = PropTypes;
