import { StyleSheet } from 'react-native';

/**
 * Colors
 */
import { COLORS } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    height: '40%',
    padding: 32,
    alignItems: 'center',
    backgroundColor: COLORS.MEDIUM_STATE_BLUE,
  },
  hero: {
    flex: 1,
  },
  landingContainer: {
    height: '60%',
    padding: 32,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  titleContainer: {
    width: 210,
  },
  title: {
    fontFamily: 'Poppins_400Regular',
    fontSize: 20,
    lineHeight: 30,
    color: COLORS.DARK_BLUE_GRAY,
  },
  subTitle: {
    fontFamily: 'Poppins_600SemiBold',
    fontSize: 20,
    lineHeight: 30,
    color: COLORS.DARK_BLUE_GRAY,
  },
  actionContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  totalConections: {
    fontFamily: 'Poppins_400Regular',
    color: COLORS.MANATEE,
    fontSize: 12,
    lineHeight: 20,
    maxWidth: 140,
  },
});

export default styles;
