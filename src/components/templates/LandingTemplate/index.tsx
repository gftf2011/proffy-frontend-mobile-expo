import React from 'react';
import { Text, View, Image } from 'react-native';

/**
 * Images
 */
import STUDY_ICON from '../../../../assets/images/icons/study.png';
import GIVE_CLASSES_ICON from '../../../../assets/images/icons/give-classes.png';
import PURPLE_HEART from '../../../../assets/images/icons/heart.png';
import LANDING_IMG from '../../../../assets/images/landing.png';

/**
 * Styles
 */
import styles from './styles';

/**
 * Molecules
 */
import LandingActionButton from '../../molecules/LandingActionButton';

/**
 * Organisms
 */
import LandingHeader from '../../organisms/LandingHeader';

/**
 * Interfaces
 */
import { LandingTemplateProps } from './interfaces';
import { COLORS } from '../../molecules/LandingActionButton/interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const LandingTemplate: React.FC<LandingTemplateProps> = ({
  totalConections = 0,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <LandingHeader />
        <Image source={LANDING_IMG} resizeMode="contain" style={styles.hero} />
      </View>
      <View style={styles.landingContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Seja bem vindo.</Text>
          <Text style={styles.subTitle}>O que deseja fazer?</Text>
        </View>
        <View style={styles.actionContainer}>
          <LandingActionButton
            route="Study"
            text="Estudar"
            buttonColor={COLORS.PRIMARY}
          >
            <Image source={STUDY_ICON} />
          </LandingActionButton>
          <LandingActionButton
            route="GiveClasses"
            text="Dar Aulas"
            buttonColor={COLORS.SECONDARY}
          >
            <Image source={GIVE_CLASSES_ICON} />
          </LandingActionButton>
        </View>
        <Text style={styles.totalConections}>
          {`Total de ${totalConections} conexões já realizadas! `}
          <Image source={PURPLE_HEART} />
        </Text>
      </View>
    </View>
  );
};

export default LandingTemplate;

LandingTemplate.propTypes = PropTypes;
