import React from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';
import { useNavigation, StackActions } from '@react-navigation/native';

/**
 * Images
 */
import SUCCESS_SIGN_IN_BACKGROUND from '../../../../assets/images/success-sign-in-background.png';
import DONE from '../../../../assets/images/done.png';

/**
 * Styles
 */
import styles from './styles';

/**
 * Atoms
 */
import Button from '../../atoms/Button';

/**
 * Interfaces
 */
import { COLORS } from '../../atoms/Button/interfaces';
import { SuccessEditingTemplateProps } from './interfaces';

/**
 * Proptypes
 */
import PropTypes from './propTypes';

const SuccessEditingTemplate: React.FC<SuccessEditingTemplateProps> = ({
  routeToGo,
}) => {
  /**
   * Navigation
   */
  const { replace } = StackActions;
  const { dispatch } = useNavigation();

  /**
   * Handlers
   */
  const handleNavigation = (): void => {
    dispatch(replace(routeToGo));
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        style={styles.backgroundImage}
        resizeMode="contain"
        source={SUCCESS_SIGN_IN_BACKGROUND}
      >
        <Image source={DONE} />
        <Text style={styles.title}>Cadastro Salvo!</Text>
        <Text style={styles.description}>
          Tudo certo, seu cadastro está na nossa lista de professores. Agora é
          só ficar de olho no seu WhatsApp!
        </Text>
      </ImageBackground>
      <Button buttonColor={COLORS.SECONDARY} onPress={handleNavigation}>
        <Text style={styles.goBackHomeText}>Voltar para o Início!</Text>
      </Button>
    </View>
  );
};

export default SuccessEditingTemplate;

SuccessEditingTemplate.propTypes = PropTypes;
