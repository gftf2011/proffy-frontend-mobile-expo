import Constants from 'expo-constants';

const endpoints = {
  backend: `http://${Constants.manifest.extra.ip}:3334`,
  ibge: `https://servicodados.ibge.gov.br/api/v1`,
};

export default endpoints;
