import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

/**
 * Molecules
 */
import Header from '../components/organisms/Header';

/**
 * Pages
 */
import Profile from '../components/pages/Profile';
import Landing from '../components/pages/Landing';
import Login from '../components/pages/Login';
import SignIn from '../components/pages/SignIn';
import SuccessSignIn from '../components/pages/SuccessSignIn';
import ForgotPassword from '../components/pages/ForgotPassword';
import SuccessForgotPassword from '../components/pages/SuccessForgotPassword';
import GiveClasses from '../components/pages/GiveClasses';
import Onboard from '../components/pages/Onboard';
import SuccessEditing from '../components/pages/SuccessEditing';

/**
 * Navigation
 */
import StudyTabs from './StudyTabs';

const { Navigator, Screen } = createStackNavigator();

const AppStack = () => {
  return (
    <NavigationContainer>
      <Navigator screenOptions={{ headerShown: false }}>
        <Screen name="Onboard" component={Onboard} />
        <Screen name="Login" component={Login} />
        <Screen name="SignIn" component={SignIn} />
        <Screen name="SuccessSignIn" component={SuccessSignIn} />
        <Screen name="SuccessEditing" component={SuccessEditing} />
        <Screen name="Landing" component={Landing} />
        <Screen name="ForgotPassword" component={ForgotPassword} />
        <Screen
          name="SuccessForgotPassword"
          component={SuccessForgotPassword}
        />
        <Screen
          name="Profile"
          component={Profile}
          options={Header('Meu Perfil')}
        />
        <Screen
          name="GiveClasses"
          component={GiveClasses}
          options={Header('Dar Aulas')}
        />
        <Screen
          name="Study"
          options={Header('Estudar')}
          component={StudyTabs}
        />
      </Navigator>
    </NavigationContainer>
  );
};

export default AppStack;
