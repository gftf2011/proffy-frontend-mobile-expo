import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

/**
 * Atoms
 */
import Icon from '../components/atoms/Icon';

/**
 * Organisms
 */
import TabBarOptions from '../components/organisms/TabBarOptions';

/**
 * Pages
 */
import TeacherList from '../components/pages/TeacherList';
import FavoriteTeachers from '../components/pages/FavoriteTeachers';

/**
 * Interfaces
 */
import { IconType } from '../components/atoms/Icon/interfaces';

const { Navigator, Screen } = createBottomTabNavigator();

const StudyTabs = () => (
  <Navigator tabBarOptions={TabBarOptions}>
    <Screen
      name="TeacherList"
      component={TeacherList}
      options={{
        tabBarLabel: 'Proffs',
        tabBarIcon: props => (
          <Icon type={IconType.IONICONS} name="ios-easel" {...props} />
        ),
      }}
    />
    <Screen
      name="FavoriteTeachers"
      component={FavoriteTeachers}
      options={{
        tabBarLabel: 'Favoritos',
        tabBarIcon: props => (
          <Icon type={IconType.IONICONS} name="ios-heart" {...props} />
        ),
      }}
    />
  </Navigator>
);

export default StudyTabs;
