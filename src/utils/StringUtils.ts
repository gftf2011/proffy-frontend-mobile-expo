export const unmaskBrlPhone = (maskedText: string): string => {
  return maskedText
    .replace('(', '')
    .replace(')', '')
    .replace(' ', '')
    .replace('-', '');
};

export const unmaskBrlCurrency = (maskedText: string): string => {
  return maskedText
    .replace(/\./g, '')
    .replace('R$', '')
    .replace(' ', '')
    .replace(',', '');
};

export const convertNumberToHourMinuteTimeFormat = (
  value: number | string,
): string => {
  const hours = Math.floor(parseInt(value.toString(), 10) / 60);
  const minutes = Math.round((parseFloat(value.toString()) / 60 - hours) * 60);
  const formattedHours = hours.toString().length > 1 ? hours : `0${hours}`;
  const formattedMinutes =
    minutes.toString().length > 1 ? minutes : `0${minutes}`;

  return `${formattedHours}:${formattedMinutes}`;
};

export const joinStringArrayByOperator = (
  operator: string,
  ...args: string[]
): string => {
  return args.map(arg => arg.trim()).join(operator);
};
