import axios, { AxiosInstance } from 'axios';
import endpoints from '../constants/endpoints';

export const defaultHeaders = () => {
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    Connection: 'close',
  };
};

export const headerWithAuthorization = (token: string) => {
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: `${token}`,
    Connection: 'close',
  };
};

export const headersFile = (token: string) => {
  return {
    'Content-Type': 'multipart/form-data',
    Authorization: `${token}`,
  };
};

export const ibgeApi = (): AxiosInstance => {
  return axios.create({
    baseURL: endpoints.ibge,
  });
};

const api = (headers?: any): AxiosInstance => {
  return axios.create({
    baseURL: endpoints.backend,
    headers,
    validateStatus: (status: number): boolean => {
      if (status >= 200 && status < 300) {
        return true;
      }
      return false;
    },
    transformResponse: res => {
      const response = JSON.parse(res);
      const { error } = response;
      if (error) {
        throw Error(error as string);
      }
      return response;
    },
  });
};

export default api;
