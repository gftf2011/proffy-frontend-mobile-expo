import { StatusBar } from 'expo-status-bar';
import React from 'react';

import 'intl';
import 'intl/locale-data/jsonp/pt-BR';

/**
 * Expo
 */
import { AppLoading } from 'expo';
import {
  Archivo_400Regular,
  Archivo_700Bold,
} from '@expo-google-fonts/archivo';
import {
  Poppins_400Regular,
  Poppins_600SemiBold,
  useFonts,
} from '@expo-google-fonts/poppins';

/**
 * Context
 */
import LoadingProvider from './src/context/LoadingContext';
import RemenberMeProvider from './src/context/RemenberMeContext';
import AuthProvider from './src/context/AuthContext';
import UserProvider from './src/context/UserContext';
import OnboardPageProvider from './src/context/OnboardPageContext';
import SignInPageProvider from './src/context/SignInPageContext';
import ClassesProvider from './src/context/ClassesContext';
import ForgotPasswordProvider from './src/context/ForgotPasswordContext';
import TeacherListProvider from './src/context/TeacherListContext';
import FavoriteTeacherListProvider from './src/context/FavoriteTeacherListContext';

import AppStack from './src/routes/AppStack';

const App = () => {
  const [fontsLoaded] = useFonts({
    Archivo_400Regular,
    Archivo_700Bold,
    Poppins_400Regular,
    Poppins_600SemiBold,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
    <LoadingProvider>
      <SignInPageProvider>
        <OnboardPageProvider>
          <RemenberMeProvider>
            <AuthProvider>
              <UserProvider>
                <ClassesProvider>
                  <ForgotPasswordProvider>
                    <TeacherListProvider>
                      <FavoriteTeacherListProvider>
                        <>
                          <AppStack />
                          <StatusBar style="light" />
                        </>
                      </FavoriteTeacherListProvider>
                    </TeacherListProvider>
                  </ForgotPasswordProvider>
                </ClassesProvider>
              </UserProvider>
            </AuthProvider>
          </RemenberMeProvider>
        </OnboardPageProvider>
      </SignInPageProvider>
    </LoadingProvider>
  );
};

export default App;
