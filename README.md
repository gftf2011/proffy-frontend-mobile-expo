<div align="center">
  <h1>
      <img alt="Proffy" title="Proffy" src=".gitlab/logo.svg" />
  </h1>
</div>
<div align="center">
  <p>
    <a href="#page_facing_up-sobre">Sobre</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="#rocket-tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="#computer-projeto">Projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="#boom-como-executar">Como Executar</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="#bookmark-layout">Layout</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="#memo-licença">Licença</a>
  </p>
</div>
<div align="center">
  <p>
    <img src="https://img.shields.io/static/v1?label=PRs&message=welcome&color=8257E5&labelColor=000000" alt="PRs welcome!" />
    <img src="https://img.shields.io/static/v1?label=license&message=MIT&color=8257E5&labelColor=000000" alt="License" />
  </p>
</div>
<br>
<div align="center">
  <p>
    <img alt="Proffy" src=".gitlab/proffy.png" width="100%">
  </p>
</div>

## :page_facing_up: Sobre

O Proffy é uma aplicação Web e Mobile feita para auxiliar na conexão entre os alunos e os professores. Logo, esta aplicação oferece aos professores a possibilidade de registrar aulas, podendo adicionar informações como a disciplina, o custo e horário e aos alunos a possibilidade de buscar pelas aulas cadastradas.

Este projeto foi idealizado pensando no 6 de agosto, onde se comemora o Dia Nacional dos Profissionais da Educação.

Essa aplicação foi realizada durante a Next Level Week #2, projeto da Rocketseat e melhorada na milha extra por [Gabriel Ferrari Tarallo Ferraz](https://www.linkedin.com/in/gabriel-ferrari-tarallo-ferraz-7a4218135/)

## :rocket: Tecnologias

Esse projeto foi desenvolvido com as seguintes tecnologias:
- [Git](https://git-scm.com/)
- [Yarn](https://yarnpkg.com/)
- [Node.js](https://nodejs.org/en/)
- [Typescript](https://www.typescriptlang.org/)
- [Expo](https://expo.io/)

## :computer: Projeto

O Proffy é uma plataforma de estudos online que ajuda pessoas a encontrarem professores online.

## :boom: Como Executar

- ### **Pré-requisitos**

  - Para executar o aplicativo em **LAN** é **necessário** que computador e celular estejam na mesma **REDE WI-FI**
  - É **necessário** possuir o **[Node.js](https://nodejs.org/en/)** instalado no computador
  - É **necessário** possuir o **[Git](https://git-scm.com/)** instalado e configurado no computador
  - Também, é **preciso** ter um gerenciador de pacotes seja o **[NPM](https://www.npmjs.com/)** ou **[Yarn](https://yarnpkg.com/)**.
  - Por fim, é **essencial** ter o **[Expo](https://expo.io/)** instalado de forma global na máquina


1. Faça um clone do repositório **Front-End Mobile**:

```sh
  $ git clone https://gitlab.com/gftf2011/proffy-frontend-mobile-expo.git
```

2. Faça um clone do repositório **Back-End**:

```sh
  ########################################################################
  # Siga as intruções de execução do Back-End antes de ir para o passo 3 #
  ########################################################################
  $ git clone https://gitlab.com/gftf2011/proffy-backend.git
```

3. Crie o arquivo **.env** com base no arquivo **.env.sample** e preencha as informações:
```env
  #######################################################################
  # Copie e cole o IP dado pelo expo na variável EXPO_PACKAGER_HOSTNAME #
  #######################################################################
  ESLINT_CONFIG_PRETTIER_NO_DEPRECATED=true
  EXPO_PACKAGER_HOSTNAME=192.168.15.6
```

4. Executando a Aplicação:

```sh
  ###################################################################################
  # É recomendado o uso do YARN, ou use o npm install para instalar as dependências #
  ###################################################################################
  $ yarn
  ####################################################
  # Inicie o projeto, ou use o comando npm run start #
  ####################################################
  $ yarn start
```

## :bookmark: Layout

Nos links abaixo você encontra o layout do projeto web e também do mobile. Lembrando que você precisa ter uma conta no [Figma](http://figma.com/) para acessá-lo.

- [Layout Web](https://www.figma.com/file/Agvethfp7FANyXDDU3LUfd/Proffy-Web-2.0)
- [Layout Mobile](https://www.figma.com/file/nZ7lMEBYZSMhRxfdvy6fKz/Proffy-Mobile-2.0)

## :memo: Licença

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](https://gitlab.com/gftf2011/proffy-frontend-mobile-expo/-/blob/master/LICENSE) para mais detalhes.

---

Feito com :heart: by [Gabriel Ferrari Tarallo Ferraz](https://www.linkedin.com/in/gabriel-ferrari-tarallo-ferraz-7a4218135/)
